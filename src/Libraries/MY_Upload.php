<?php

namespace Yuan116\Ci3\Enhance\Libraries;

defined('CI_VERSION') or die('Only for Codeigniter 3 (CI3)');

use League\Flysystem\{
    Filesystem,
    UnableToCreateDirectory
};
use League\Flysystem\Local\LocalFilesystemAdapter;

use RuntimeException;

class MY_Upload extends \CI_Upload
{
    protected string $root = FCPATH;
    protected Filesystem $file_system;

    protected array $multi_data = [];

    protected string $file_size_error = 'upload_file_exceeds_limit';
    protected string $file_type_error = 'upload_invalid_filetype';

    public function __construct(array $config = [])
    {
        $this->file_system = new Filesystem(new LocalFilesystemAdapter($this->root));

        parent::__construct($config);
        $this->setFileSizeError();
        $this->setFileTypeAllowError();
    }

    /**
     * {@inheritDoc}
     */
    public function initialize(array $config = [], $reset = TRUE)
    {
        parent::initialize($config, $reset);

        if (!empty($config['upload_path'])) {
            $this->createDir($config['upload_path']);
        }
    }

    protected function createDir($directory): void
    {
        if (!$this->file_system->fileExists($directory)) {
            try {
                $this->file_system->createDirectory($directory);
            } catch (UnableToCreateDirectory $exception) {
                show_error("Unable to create '{$directory}' directory, please manually create '{$directory}'.");
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    public function data($index = NULL)
    {
        $data = [
            'file_name'         => $this->file_name,
            'file_type'         => $this->file_type,
            'file_path'         => $this->upload_path,
            'full_path'         => $this->upload_path . $this->file_name,
            'raw_name'          => str_replace($this->file_ext, '', $this->file_name),
            'orig_name'         => $this->orig_name,
            'client_name'       => $this->client_name,
            'file_ext'          => $this->file_ext,
            'file_size'         => $this->file_size,
            'is_image'          => $this->is_image(),
            'image_width'       => $this->image_width,
            'image_height'      => $this->image_height,
            'image_type'        => $this->image_type,
            'image_size_str'    => $this->image_size_str,
        ];

        if (!empty($index)) {
            if (is_array($index)) {
                $final_data = [];
                foreach ($index as $key) {
                    $final_data[$key] = $data[$key];
                }

                return $final_data;
            }

            return $data[$index];
        }

        return $data;
    }

    /**
     * {@inheritDoc}
     */
    protected function _file_mime_type($file, int $count = NULL)
    {
        if ($count === NULL) {
            $tmp_name = $file['tmp_name'];
            $type = $file['type'];
        } else {
            $tmp_name = $file['tmp_name'][$count];
            $type = $file['type'][$count];
        }

        $regexp = "/^([a-z\-]+\/[a-z0-9\-\.\+]+)(;\s.+)?$/";

        if (function_exists('finfo_file')) {
            $finfo = finfo_open(FILEINFO_MIME);
            if (is_resource($finfo)) {
                $mime = @finfo_file($finfo, $tmp_name);
                finfo_close($finfo);

                if (is_string($mime) && preg_match($regexp, $mime, $matches)) {
                    $this->file_type = $matches[1];
                    return;
                }
            }
        }

        if (DIRECTORY_SEPARATOR !== "\\") {
            $cmd = function_exists('escapeshellarg')
                ? 'file --brief --mime ' . escapeshellarg($tmp_name) . ' 2>&1'
                : 'file --brief --mime ' . $tmp_name . ' 2>&1';

            if (function_usable("exec")) {
                $mime = @exec($cmd, $mime, $return_status);
                if ($return_status === 0 && is_string($mime) && preg_match($regexp, $mime, $matches)) {
                    $this->file_type = $matches[1];
                    return;
                }
            }
        }

        if (!ini_get('safe_mode') && function_usable('shell_exec')) {
            $mime = @shell_exec($cmd);
            if (strlen($mime) > 0) {
                $mime = explode("\n", trim($mime));
                if (preg_match($regexp, $mime[(count($mime) - 1)], $matches)) {
                    $this->file_type = $matches[1];
                    return;
                }
            }
        }

        if (function_usable('popen')) {
            $proc = @popen($cmd, "r");
            if (is_resource($proc)) {
                $mime = @fread($proc, 512);
                @pclose($proc);
                if ($mime !== FALSE) {
                    $mime = explode("\n", trim($mime));
                    if (preg_match($regexp, $mime[(count($mime) - 1)], $matches)) {
                        $this->file_type = $matches[1];
                        return;
                    }
                }
            }
        }

        if (function_exists('mime_content_type')) {
            $this->file_type = @mime_content_type($tmp_name);
            if (strlen($this->file_type) > 0) {
                return;
            }
        }

        $this->file_type = $type;
    }

    protected function setMultiData(): void
    {
        $this->multi_data[] = [
            'file_name'         => $this->file_name,
            'file_type'         => $this->file_type,
            'file_path'         => $this->upload_path,
            'full_path'         => $this->upload_path . $this->file_name,
            'raw_name'          => str_replace($this->file_ext, '', $this->file_name),
            'orig_name'         => $this->orig_name,
            'client_name'       => $this->client_name,
            'file_ext'          => $this->file_ext,
            'file_size'         => $this->file_size,
            'is_image'          => $this->is_image(),
            'image_width'       => $this->image_width,
            'image_height'      => $this->image_height,
            'image_type'        => $this->image_type,
            'image_size_str'    => $this->image_size_str,
        ];
    }

    public function getMultiData($index = NULL): array
    {
        if (!empty($index)) {
            if (is_array($index)) {
                $final_data = [];
                foreach ($this->multi_data as $key => $data) {
                    foreach ($index as $value) {
                        $final_data[$key][$value] = $data[$value];
                    }
                }

                return $final_data;
            }

            return array_column($this->multi_data, $index);
        }

        return $this->multi_data;
    }

    public function doMultiUpload($field): bool
    {
        if (!is_array($_FILES[$field]['name'])) {
            throw new RuntimeException("\$_FILES[{$field}] is not an array.");
        }

        if (!$this->validate_upload_path()) {
            return FALSE;
        }

        $_file = $_FILES[$field];

        foreach (array_keys($_file['name']) as $index) {
            if (!is_uploaded_file($_file['tmp_name'][$index])) {
                $error = $_file['error'][$index] ?? UPLOAD_ERR_NO_FILE;

                switch ($error) {
                    case UPLOAD_ERR_INI_SIZE:
                        ini_get('upload_max_filesize');
                        $this->fileSizeError();
                        break;
                    case UPLOAD_ERR_FORM_SIZE:
                        $this->set_error('upload_file_exceeds_form_limit', 'info');
                        break;
                    case UPLOAD_ERR_PARTIAL:
                        $this->set_error('upload_file_partial', 'debug');
                        break;
                    case UPLOAD_ERR_NO_FILE:
                        $this->set_error('upload_no_file_selected', 'debug');
                        break;
                    case UPLOAD_ERR_NO_TMP_DIR:
                        $this->set_error('upload_no_temp_directory', 'error');
                        break;
                    case UPLOAD_ERR_CANT_WRITE:
                        $this->set_error('upload_unable_to_write_file', 'error');
                        break;
                    case UPLOAD_ERR_EXTENSION:
                        $this->set_error('upload_stopped_by_extension', 'debug');
                        break;
                    default:
                        $this->set_error('upload_no_file_selected', 'debug');
                        break;
                }

                return FALSE;
            }

            $this->file_temp = $_file['tmp_name'][$index];
            $this->file_size = $_file['size'][$index];
            $this->_file_mime_type($_file, $index);
            $this->file_type = preg_replace("/^(.+?);.*$/", "\\1", $this->file_type);
            $this->file_type = strtolower(trim(stripslashes($this->file_type), '"'));
            $this->file_name = $this->_prep_filename($_file['name'][$index]);
            $this->file_ext = $this->get_extension($this->file_name);
            $this->client_name = $this->file_name;

            if (!$this->is_allowed_filetype()) {
                $this->fileTypeError();
                return FALSE;
            }

            if ($this->_file_name_override !== '') {
                $this->file_name = $this->_prep_filename($this->_multi_file_name_override[$index]);

                if (strpos($this->_multi_file_name_override[$index], ".") === FALSE) {
                    $this->file_name .= $this->file_ext;
                } else {
                    $this->file_ext = $this->get_extension($this->_multi_file_name_override[$index]);
                }

                if (!$this->is_allowed_filetype(TRUE)) {
                    $this->fileTypeError();
                    return FALSE;
                }
            }

            if ($this->file_size > 0) {
                $this->file_size = round($this->file_size / 1024, 2);
            }

            if (!$this->is_allowed_filesize()) {
                $this->fileSizeError();
                return FALSE;
            }

            if (!$this->is_allowed_dimensions()) {
                $this->set_error('upload_invalid_dimensions', 'info');
                return FALSE;
            }

            $this->file_name = $this->_CI->security->sanitize_filename($this->file_name);

            if ($this->max_filename > 0) {
                $this->file_name = $this->limit_filename_length($this->file_name, $this->max_filename);
            }

            if ($this->remove_spaces) {
                $this->file_name = preg_replace("/\s+/", "_", $this->file_name);
            }

            if ($this->file_ext_tolower && ($ext_length = strlen($this->file_ext))) {
                $this->file_name = substr($this->file_name, 0, -$ext_length) . $this->file_ext;
            }

            $this->orig_name = $this->file_name;
            if (($this->file_name = $this->set_filename($this->upload_path, $this->file_name)) === FALSE) {
                return FALSE;
            }

            if ($this->xss_clean && $this->do_xss_clean() === FALSE) {
                $this->set_error('upload_unable_to_write_file');
                return FALSE;
            }

            if (!@copy($this->file_temp, $this->upload_path . $this->file_name)) {
                if (!@move_uploaded_file($this->file_temp, $this->upload_path . $this->file_name)) {
                    $this->set_error('upload_invalid_dimensions', 'info');
                    return FALSE;
                }
            }

            $this->set_image_properties($this->upload_path . $this->file_name);

            $this->setMultiData();
        }

        return TRUE;
    }

    protected function setFileSizeError()
    {
        $this->file_size_error = 'Maximum allow file size (' . $this->getAllowFileSize() . ')';
        return $this;
    }

    protected function setFileTypeAllowError()
    {
        switch ($this->allowed_types) {
            case '*':
                $this->file_type_error = 'upload_invalid_filetype';
                break;
            default:
                $this->file_type_error = 'File type allow to upload (' . implode(', ', (array) $this->allowed_types) . ')';
                break;
        }

        return $this;
    }

    public function getAllowFileSize(): string
    {
        $size = $this->max_size > 0 ? $this->max_size . 'k' :  ini_get('upload_max_filesize');

        $bytes = (int) $size;

        switch (strtolower(substr($size, -1))) {
            case 'g':
                $bytes *= 1024;
            case 'm':
                $bytes *= 1024;
            case 'k':
                $bytes *= 1024;
        }

        $i = floor(log($bytes, 1024));
        return round($bytes / pow(1024, $i)) . ['B', 'kB', 'MB', 'GB'][$i];
    }

    protected function fileSizeError()
    {
        return $this->set_error($this->file_size_error, 'info');
    }

    protected function fileTypeError()
    {
        return $this->set_error($this->file_type_error, 'debug');
    }
}
