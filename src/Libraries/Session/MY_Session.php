<?php

namespace Yuan116\Ci3\Enhance\Libraries\Session;

defined('CI_VERSION') or die('Only for Codeigniter 3 (CI3)');

class MY_Session extends \CI_Session
{
    protected $ci;

    protected $cookie_name = '';
    protected $cookie_prefix = '';
    protected $remember_me_name = '';

    public function __construct()
    {
        parent::__construct();
        $this->ci = get_instance();

        $this->cookie_name = $this->ci->config->item('sess_cookie_name');
        $this->cookie_prefix = $this->ci->config->item('cookie_prefix');

        if (empty($this->remember_me_name)) {
            $this->remember_me_name = $this->cookie_name . '_remember_me';
        }
    }

    public function replaceCICookie(int $expire = 1209600): void
    {
        $this->ci->input->set_cookie([
            'name' => $this->cookie_name,
            'value' => $this->ci->input->cookie($this->cookie_prefix . $this->cookie_name),
            'expire' => time() + $expire,
            'prefix' => $this->cookie_prefix
        ]);
    }

    public function setRememberMe($value, int $expire = 1209600): void
    {
        $this->ci->input->set_cookie([
            'name' => $this->remember_me_name,
            'value' => $value,
            'expire' => time() + $expire,
            'prefix' => $this->cookie_prefix
        ]);
    }

    public function getRememberMe(): string
    {
        return $this->ci->input->cookie($this->cookie_prefix . $this->remember_me_name);
    }

    public function hasRememberMe(): bool
    {
        return empty($this->getRememberMe()) ? FALSE : TRUE;
    }

    public function redirectRememberMe(string $redirect): void
    {
        if ($this->hasRememberMe()) {
            redirect($redirect);
        }
    }

    public function deleteRememberMe(): void
    {
        $this->ci->input->set_cookie([
            'name' => $this->remember_me_name,
            'value' => '',
            'expire' => time() - 3600,
            'prefix' => $this->cookie_prefix
        ]);
    }
}
