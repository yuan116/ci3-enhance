<?php

namespace Yuan116\Ci3\Enhance\Libraries\Session\Drivers;

defined('CI_VERSION') or die('Only for Codeigniter 3 (CI3)');

class MY_Session_database_driver extends \CI_Session_database_driver
{
    protected $ci;

    public function __construct(&$params)
    {
        parent::__construct($params);
        $this->ci = get_instance();
        $this->createSessionTable();
    }

    protected function createSessionTable(): void
    {
        $table = $this->_config['save_path'];
        if (!$this->ci->db->table_exists($table)) {
            $this->ci->load->dbforge();
            $this->ci->dbforge
                ->add_field([
                    'id' => [
                        'type' => 'VARCHAR',
                        'constraint' => 128
                    ],
                    'ip_address' => [
                        'type' => 'VARCHAR',
                        'constraint' => 45
                    ],
                    'timestamp' => [
                        'type' => 'BIGINT',
                        'default' => '0',
                        'unsigned' => TRUE
                    ],
                    'data' => [
                        'type' => 'TEXT',
                        'default' => ''
                    ],
                ])
                ->add_key('id', TRUE)
                ->add_key('timestamp');

            if ($this->_config['match_ip']) {
                $this->ci->dbforge->add_key('ip_address', TRUE);
            }

            $this->ci->dbforge->create_table($table);
        }
    }
}
