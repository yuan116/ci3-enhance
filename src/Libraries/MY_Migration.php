<?php

namespace Yuan116\Ci3\Enhance\Libraries;

defined('CI_VERSION') or die('Only for Codeigniter 3 (CI3)');

use RuntimeException;
use UnexpectedValueException;

class MY_Migration extends \CI_Migration
{
    //override parent constructor
    public function __construct()
    {
        // Only run this constructor on main library load
        if (!in_array(get_class($this), ['CI_Migration', config_item('subclass_prefix') . 'Migration', 'Yuan116\Ci3\Enhance\Libraries\MY_Migration'], TRUE)) {
            return;
        }

        $this->config->load('migration', TRUE);
        foreach ($this->config->item('migration') as $key => $val) {
            $this->{"_{$key}"} = $val;
        }

        // Are they trying to use migrations while it is disabled?
        if ($this->_migration_enabled !== TRUE) {
            throw new RuntimeException('Migrations has been loaded but is disabled or set up incorrectly.');
        }

        // If not set, set it
        $this->_migration_path !== '' or $this->_migration_path = APPPATH . 'migrations';

        // Add trailing slash if not set
        $this->_migration_path = rtrim($this->_migration_path, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;

        // Load migration language
        $this->lang->load('migration');

        // They'll probably be using dbforge
        $this->load->database();
        $this->load->dbforge();

        // Make sure the migration table name was set.
        if (empty(trim($this->_migration_table))) {
            throw new RuntimeException('Migrations configuration file (migration.php) must have "migration_table" set.');
        }

        // Migration basename regex
        $this->_migration_regex = ($this->_migration_type === 'timestamp')
            ? '/^\d{14}_(\w+)$/'
            : '/^\d{3}_(\w+)$/';

        // Make sure a valid migration numbering type was set.
        if (!in_array($this->_migration_type, ['sequential', 'timestamp'])) {
            throw new UnexpectedValueException('An invalid migration numbering type was specified: ' . $this->_migration_type);
        }

        // If the migrations table is missing, make it
        // If exist, check its fields
        $this->checkMigrationTable();

        ini_set('memory_limit', -1);
        ini_set('max_execution_time', 0);
        $this->setForeignKeyCheck(FALSE);
    }

    public function __destruct()
    {
        $this->setForeignKeyCheck(TRUE);
    }

    public function checkMigrationTable(): void
    {
        if (!$this->db->table_exists($this->_migration_table)) {
            echo "Migration table ({$this->_migration_table}) is missing." . PHP_EOL;
            $this->createMigrationTable();
        } else {
            if ($this->db->field_exists('version', $this->_migration_table)) {
                $old_migration_table = $this->_migration_table . '_old';

                echo "Detected using CI default migration library. we rename it to '{$old_migration_table}' and create a new migration table." . PHP_EOL;
                $this->dbforge->rename_table($this->_migration_table, $old_migration_table);
                echo "Renamed to '{$old_migration_table}'." . PHP_EOL;

                $this->createMigrationTable();

                $version = $this->db->get($old_migration_table)->row()->version ?? 0;
                $migration_file_list = $this->find_migrations();

                $migration_file_list = array_filter($migration_file_list, function ($key) use ($version) {
                    return $key <= $version;
                }, ARRAY_FILTER_USE_KEY);

                $count = count($migration_file_list);
                echo "Found {$count} migration ran before." . PHP_EOL;

                if (!empty($migration_file_list)) {
                    $data = [
                        'batch' => 1,
                        'datetime' => date('Y-m-d H:i:s')
                    ];
                    foreach ($migration_file_list as $filename) {
                        $data['filename'] = $filename;
                        $this->run($data);
                    }

                    echo "Completed migrate {$count} migration(s)." . PHP_EOL;
                }
            }
        }
    }

    protected function createMigrationTable(): void
    {
        echo "Creating new migration table ({$this->_migration_table})." . PHP_EOL;

        $start = microtime(TRUE);
        $this->dbforge->add_field($this->getMigrationField())
            ->add_key('id', TRUE)
            ->create_table($this->_migration_table);
        $end = microtime(TRUE) - $start;

        echo "Successfully created migration table ({$this->_migration_table}). {$end}ms" . PHP_EOL;
    }

    protected function getMigrationField(): array
    {
        return [
            'id' => [
                'type' => 'BIGINT',
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'filename' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'batch' => [
                'type' => 'INT'
            ],
            'datetime' => [
                'type' => 'DATETIME'
            ]
        ];
    }

    public function getMigrationType(): string
    {
        return $this->_migration_type;
    }

    public function getMigrationFileList(): array
    {
        $file_list = [];

        foreach (glob($this->_migration_path . '*_*.php') as $file) {
            $name = basename($file, '.php');

            // Filter out non-migration files
            if (preg_match($this->_migration_regex, $name)) {
                $number = $this->_get_migration_number($name);

                // There cannot be duplicate migration numbers
                if (isset($file_list[$number])) {
                    $this->_error_string = sprintf($this->lang->line('migration_multiple_version'), $number);
                    show_error($this->_error_string);
                }

                $file_list[$number] = $name;
            }
        }

        ksort($file_list);
        return $file_list;
    }

    public function getMigrationList(array $where = NULL): array
    {
        return $this->db->get_where($this->_migration_table, $where)->result();
    }

    public function getBatch(): int
    {
        $row = $this->db->select_max('batch', 'batch')->get($this->_migration_table)->row();

        return (int) $row->batch ?? 0;
    }

    public function getMigrationPath(): string
    {
        return $this->_migration_path;
    }

    public function run($data): bool
    {
        return $this->db->insert($this->_migration_table, $data);
    }

    public function rollback(array $where): bool
    {
        return $this->db->delete($this->_migration_table, $where);
    }

    public function getMigrationTable(): string
    {
        return $this->_migration_table;
    }

    public function getTableList(): array
    {
        return $this->db->list_tables();
    }

    public function setForeignKeyCheck(bool $val)
    {
        $check = $val ? '1' : '0';

        return $this->db->query("SET FOREIGN_KEY_CHECKS={$check};");
    }

    public function dropTable($table): bool
    {
        return $this->dbforge->drop_table($table, TRUE);
    }

    public function transStart(): bool
    {
        $this->db->trans_strict(FALSE);

        return $this->db->trans_start();
    }

    public function transCommit(): bool
    {
        return $this->db->trans_commit();
    }

    public function transRollback(): bool
    {
        return $this->db->trans_rollback();
    }
}
