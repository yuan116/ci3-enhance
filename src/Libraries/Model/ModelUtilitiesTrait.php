<?php

namespace Yuan116\Ci3\Enhance\Libraries\Model;

defined('CI_VERSION') or die('Only for Codeigniter 3 (CI3)');

trait ModelUtilitiesTrait
{
    public function optimizeTable()
    {
        return $this->dbutil->optimize_table($this->table);
    }

    public function repairTable()
    {
        return $this->dbutil->repair_table($this->table);
    }

    public function countAll(): int
    {
        return $this->db->count_all($this->table);
    }

    public function fieldData(): array
    {
        return $this->db->field_data($this->table);
    }

    public function listFields(): array
    {
        return $this->db->list_fields($this->table);
    }
}
