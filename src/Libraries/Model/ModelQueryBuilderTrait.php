<?php

namespace Yuan116\Ci3\Enhance\Libraries\Model;

defined('CI_VERSION') or die('Only for Codeigniter 3 (CI3)');

use InvalidArgumentException;

trait ModelQueryBuilderTrait
{
    public function set($key, $value = '', $escape = NULL)
    {
        if (is_array($key) || is_object($key)) {
            foreach ($key as $index => $value) {
                $this->set($index, $value, $escape);
            }
        } else if ($this->checkProtectedFields($key)) {
            return $this;
        } else {
            $this->db->set($key, $value, $escape);
        }

        return $this;
    }

    protected function checkProtectedFields($key): bool
    {
        return in_array($key, $this->proctected_fields);
    }

    public function insert(array $data): bool
    {
        if ($this->has_timestamp) {
            $this->db->set($this->created_columns);
        }

        return $this->db->insert($this->table, $data);
    }

    public function update(array $data = [], $where = NULL): bool
    {
        $this->set($data);
        $this->setWhere($where);
        if ($this->has_timestamp) {
            $this->db->set($this->updated_columns);
        }

        return $this->db->update($this->table);
    }

    public function replace(array $data): bool
    {
        return $this->db->replace($this->table, $data);
    }

    public function delete($keys = NULL): bool
    {
        $this->setWhere($keys);

        if ($this->soft_delete) {
            return $this->db->update($this->table, $this->deleted_columns);
        } else {
            return $this->db->delete($this->table);
        }
    }

    public function truncate(): bool
    {
        return $this->db->truncate($this->table);
    }

    public function select(string $select, bool $escape = NULL)
    {
        $this->db->select($select, $escape);

        return $this;
    }

    public function selectAvg(string $select, string $alias = '')
    {
        $this->db->select_avg($select, $alias);

        return $this;
    }

    public function selectMax(string $select, string $alias = '')
    {
        $this->db->select_max($select, $alias);

        return $this;
    }

    public function selectMin(string $select, string $alias = '')
    {
        $this->db->select_min($select, $alias);

        return $this;
    }

    public function selectSum(string $select, string $alias = '')
    {
        $this->db->select_sum($select, $alias);

        return $this;
    }

    public function distinct(bool $val = TRUE)
    {
        $this->db->distinct($val);

        return $this;
    }

    public function from($table)
    {
        $this->db->from($table);

        $this->has_call_from = TRUE;

        return $this;
    }

    public function join(string $table, string $condition, string $type = '', bool $escape = NULL)
    {
        $this->db->join($table, $condition, $type, $escape);

        return $this;
    }

    protected function setWhere($where)
    {
        if (!empty($where)) {
            return (is_array($where) ? $this->where($where) : $this->wherePk($where));
        }
    }

    public function where($key, $value = NULL, bool $escape = NULL)
    {
        if ($key === NULL) {
            throw new InvalidArgumentException('First parameter: $key cannot be NULL value.');
        }

        $this->db->where($key, $value, $escape);

        return $this;
    }

    public function whereIn($key, array $value = [], bool $escape = NULL)
    {
        if ($key === NULL) {
            throw new InvalidArgumentException('First parameter: $key cannot be NULL value.');
        }

        if (!empty($value)) {
            $this->db->where_in($key, $value, $escape);
        } else if (is_array($key)) {
            $this->whereIn($this->getPrimaryKey(), $key, $escape);
        }

        return $this;
    }

    public function whereNotIn($key, array $value = [], bool $escape = NULL)
    {
        if ($key === NULL) {
            throw new InvalidArgumentException('First parameter: $key cannot be NULL value.');
        }

        if (!empty($value)) {
            $this->db->where_not_in($key, $value, $escape);
        } else if (is_array($key)) {
            $this->whereNotIn($this->getPrimaryKey(), $key, $escape);
        }

        return $this;
    }

    public function wherePk($value)
    {
        return $this->where($this->getPrimaryKey(), $value);
    }

    public function orWhere($key, $value = NULL, bool $escape = NULL)
    {
        if ($key === NULL) {
            throw new InvalidArgumentException('First parameter: $key cannot be NULL value.');
        }

        $this->db->or_where($key, $value, $escape);

        return $this;
    }

    public function orWhereIn($key, array $value = [], bool $escape = NULL)
    {
        if ($key === NULL) {
            throw new InvalidArgumentException('First parameter: $key cannot be NULL value.');
        }

        if (!empty($value)) {
            $this->db->or_where_in($key, $value, $escape);
        } else if (is_array($key)) {
            $this->orWhereIn($this->getPrimaryKey(), $key, $escape);
        }

        return $this;
    }

    public function orWhereNotIn($key, array $value = [], bool $escape = NULL)
    {
        if ($key === NULL) {
            throw new InvalidArgumentException('First parameter: $key cannot be NULL value.');
        }

        if (!empty($value)) {
            $this->db->or_where_not_in($key, $value, $escape);
        } else if (is_array($key)) {
            $this->orWhereNotIn($this->getPrimaryKey(), $key, $escape);
        }

        return $this;
    }

    public function groupStart()
    {
        $this->db->group_start();

        return $this;
    }

    public function notGroupStart()
    {
        $this->db->not_group_start();

        return $this;
    }

    public function orGroupStart()
    {
        $this->db->or_group_start();

        return $this;
    }

    public function orNotGroupStart()
    {
        $this->db->or_not_group_start();

        return $this;
    }

    public function groupEnd()
    {
        $this->db->group_end();

        return $this;
    }

    public function like($field, string $match = '', string $side = 'both', bool $escape = NULL)
    {
        $this->db->like($field, $match, $side, $escape);

        return $this;
    }

    public function notLike($field, string $match = '', string $side = 'both', bool $escape = NULL)
    {
        $this->db->not_like($field, $match, $side, $escape);

        return $this;
    }

    public function orLike($field, string $match = '', string $side = 'both', bool $escape = NULL)
    {
        $this->db->or_like($field, $match, $side, $escape);

        return $this;
    }

    public function orNotLike($field, string $match = '', string $side = 'both', bool $escape = NULL)
    {
        $this->db->or_not_like($field, $match, $side, $escape);

        return $this;
    }

    public function having($key, $value = NULL, bool $escape = NULL)
    {
        $this->db->having($key, $value, $escape);

        return $this;
    }

    public function orHaving($key, $value = NULL, bool $escape = NULL)
    {
        $this->db->or_having($key, $value, $escape);

        return $this;
    }

    public function groupBy($group_by, bool $escape = NULL)
    {
        $this->db->group_by($group_by, $escape);

        return $this;
    }

    public function orderBy(string $keys, string $order = 'asc', bool $escape = NULL)
    {
        $this->db->order_by($keys, $order, $escape);

        return $this;
    }

    public function limit(int $limit, int $offset = 0)
    {
        $this->db->limit($limit, $offset);

        return $this;
    }

    public function offset(int $offset)
    {
        $this->db->offset($offset);

        return $this;
    }

    public function countAllResult(bool $reset = TRUE): int
    {
        return $this->db->count_all_results($this->has_call_from ? '' : $this->table, $reset);
    }

    public function getCompiledSelect(bool $reset = TRUE): string
    {
        return $this->db->get_compiled_select($this->has_call_from ? '' : $this->table, $reset);
    }

    public function getCompiledInsert(bool $reset = TRUE): string
    {
        return $this->db->get_compiled_insert($this->has_call_from ? '' : $this->table, $reset);
    }

    public function getCompiledUpdate(bool $reset = TRUE): string
    {
        return $this->db->get_compiled_update($this->has_call_from ? '' : $this->table, $reset);
    }

    public function getCompiledDelete(bool $reset = TRUE): string
    {
        return $this->db->get_compiled_delete($this->has_call_from ? '' : $this->table, $reset);
    }
}
