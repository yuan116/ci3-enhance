<?php

namespace Yuan116\Ci3\Enhance\Libraries\Model;

defined('CI_VERSION') or die('Only for Codeigniter 3 (CI3)');

use CI_DB_result;

trait ModelResultTrait
{
    public function get($where = NULL, int $limit = NULL, int $offset = 0): CI_DB_result
    {
        $this->softDelete()->setWhere($where);

        return $this->db->get(($this->has_call_from ? '' : $this->table), $limit, $offset);
    }

    public function result($where = NULL, int $limit = NULL, int $offset = 0, string $type = 'result_object'): array
    {
        return $this->get($where, $limit, $offset)->$type();
    }

    public function resultArray($where = NULL, int $limit = NULL, int $offset = 0): array
    {
        return $this->result($where, $limit, $offset, 'result_array');
    }

    public function resultObject($where = NULL, int $limit = NULL, int $offset = 0): array
    {
        return $this->result($where, $limit, $offset);
    }

    public function row($where = NULL, int $limit = NULL, int $offset = 0, string $type = 'row_object')
    {
        return $this->get($where, $limit, $offset)->$type();
    }

    public function rowObject($where = NULL, int $limit = NULL, int $offset = 0)
    {
        return $this->row($where, $limit, $offset);
    }

    public function rowArray($where = NULL, int $limit = NULL, int $offset = 0)
    {
        return $this->row($where, $limit, $offset, 'row_array');
    }

    public function numRows($where = NULL, int $limit = NULL, int $offset = 0): int
    {
        return $this->select($this->primary_key)->get($where, $limit, $offset)->num_rows();
    }

    public function dataSeek($where = NULL, int $limit = NULL, int $offset = 0): bool
    {
        return $this->select($this->primary_key)->get($where, $limit, $offset)->data_seek();
    }
}
