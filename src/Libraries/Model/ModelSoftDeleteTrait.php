<?php

namespace Yuan116\Ci3\Enhance\Libraries\Model;

defined('CI_VERSION') or die('Only for Codeigniter 3 (CI3)');

trait ModelSoftDeleteTrait
{
    public function softDelete(string $alias = '')
    {
        if ($this->soft_delete) {
            $alias = ($alias !== '' ? $alias : ($this->alias !== '' ? $this->alias : $this->table));
            $this->db->where(rtrim($alias . '.' . static::DELETED_AT, '.'));
        }

        return $this;
    }
}
