<?php

namespace Yuan116\Ci3\Enhance\Libraries\Model;

defined('CI_VERSION') or die('Only for Codeigniter 3 (CI3)');

trait ModelManipulationTrait
{
    public function createTable(bool $if_not_exists = FALSE, array $attributes = []): bool
    {
        if ($this->has_timestamp) {
            $this->addTimestamp();
        }

        if ($this->soft_delete) {
            $this->addSoftDelete();
        }

        return $this->dbforge->create_table($this->table, $if_not_exists, $attributes);
    }

    public function dropTable(bool $if_exists = FALSE): bool
    {
        return $this->dbforge->drop_table($this->table, $if_exists);
    }

    public function renameTable(string $old_table, string $new_table): bool
    {
        return $this->dbforge->rename_table($old_table, $new_table);
    }

    public function addField($field)
    {
        $this->dbforge->add_field($field);

        return $this;
    }

    public function addKey($field, bool $primary = FALSE)
    {
        $this->dbforge->add_key($field, $primary);

        return $this;
    }

    public function addColumn(array $field, string $after = NULL): bool
    {
        return $this->dbforge->add_column($this->table, $field, $after);
    }

    public function modifyColumn(array $field): bool
    {
        return $this->dbforge->modify_column($this->table, $field);
    }

    public function dropColumn(string $field): bool
    {
        return $this->dbforge->drop_column($this->table, $field);
    }

    public function tableExists(): bool
    {
        return $this->db->table_exists($this->table);
    }

    public function fieldExists(string $field)
    {
        return $this->db->field_exists($field, $this->table);
    }

    public function addTimestamp()
    {
        $type = $this->getDateDataType();

        $timestamp_field = [
            static::CREATED_AT => [
                'type' => $type,
                'null' => TRUE
            ],
            static::UPDATED_AT => [
                'type' => $type,
                'null' => TRUE
            ]
        ];

        if ($this->db->table_exists($this->table)) {
            $this->dbforge->add_column($this->table, $timestamp_field);
        } else {
            $this->dbforge->add_field($timestamp_field);
        }

        return $this;
    }

    public function addSoftDelete()
    {
        $delete_field = [
            static::DELETED_AT => [
                'type' => $this->getDateDataType(),
                'null' => TRUE
            ]
        ];

        if ($this->db->table_exists($this->table)) {
            $this->dbforge->add_column($this->table, $delete_field);
        } else {
            $this->dbforge->add_field($delete_field);
        }

        return $this;
    }

    protected function getDateDataType(): string
    {
        switch ($this->date_format) {
            case 'datetime':
                $type = 'DATETIME';
                break;
            case 'date':
                $type = 'DATE';
                break;
            case 'int':
            default:
                $type = 'TIMESTAMP';
                break;
        }

        return $type;
    }

    public function setForeignKeyCheck(bool $val)
    {
        $check = $val ? '1' : '0';

        return $this->db->query("SET FOREIGN_KEY_CHECKS={$check};");
    }

    public function addForeignKey(string $foreign_key, string $reference_table, string $reference_table_id, string $on_delete = '', string $on_update = '')
    {
        foreach (['delete', 'update'] as $var) {
            if (!empty(trim(${"on_{$var}"}))) {
                ${"on_{$var}"} = ' ON ' . strtoupper($var) . ' ' . strtoupper(${"on_{$var}"});
            }
        }

        $str = "CONSTRAINT `{$this->table}_{$foreign_key}_fk` FOREIGN KEY(`{$foreign_key}`) REFERENCES `{$reference_table}`(`{$reference_table_id}`)" . $on_delete . $on_update;

        if ($this->db->table_exists($this->table)) {
            $this->dbforge->add_column($this->table, $str);
        } else {
            $this->dbforge->add_field($str);
        }

        return $this;
    }

    public function dropForeignKey($foreign_key)
    {
        $table = $this->table;
        $this->db->query("ALTER TABLE {$table} DROP FOREIGN KEY {$table}_{$foreign_key}_fk");

        return $this;
    }
}
