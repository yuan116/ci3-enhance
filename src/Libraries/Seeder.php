<?php

namespace Yuan116\Ci3\Enhance\Libraries;

defined('CI_VERSION') or die('Only for Codeigniter 3 (CI3)');

use League\Flysystem\{
    Filesystem,
    UnableToCreateDirectory
};
use League\Flysystem\Local\LocalFilesystemAdapter;

class Seeder
{
    protected array $class_list = [];
    protected string $seeder_path = 'seeders';

    public function __construct()
    {
        $this->load->database();
        $this->load->dbforge();

        $this->file_system = new Filesystem(new LocalFilesystemAdapter(APPPATH));

        if (!$this->file_system->fileExists($this->seeder_path)) {
            try {
                $this->file_system->createDirectory($this->seeder_path);
            } catch (UnableToCreateDirectory $exception) {
                show_error('Unable to create \'seeders\' directory, please manually create \'seeders\' directory inside application directory.');
            }
        }
    }

    public function __get($name)
    {
        return get_instance()->$name;
    }

    public function getSeederPath(): string
    {
        return $this->seeder_path;
    }

    public function call($classes): void
    {
        $this->class_list = (array) $classes;
    }

    public function getClassList(): array
    {
        return $this->class_list;
    }
}
