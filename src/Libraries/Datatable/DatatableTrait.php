<?php

namespace Yuan116\Ci3\Enhance\Libraries\Datatable;

defined('CI_VERSION') or die('Only for Codeigniter 3 (CI3)');

trait DatatableTrait
{
    protected array $dt_select = [];
    protected string $dt_from = '';
    protected array $dt_join = [];
    protected array $dt_where = [];
    protected array $dt_where_in = [];
    protected array $dt_where_not_in = [];
    protected array $dt_or_where = [];
    protected array $dt_or_where_in = [];
    protected array $dt_or_where_not_in = [];
    protected array $dt_like = [];
    protected array $dt_not_like = [];
    protected array $dt_or_like = [];
    protected array $dt_or_not_like = [];
    protected array $dt_group_by = [];
    protected array $dt_having = [];
    protected array $dt_or_having = [];
    protected array $dt_order_by = [];
    protected int $dt_limit = 0;
    protected int $dt_offset = 0;

    protected array $dt_request = [
        'draw' => 0,
        'columns' => [],
        'order' => [],
        'length' => 10,
        'start' => 0,
        'search' => [
            'value' => ''
        ]
    ];

    protected array $dt_filter_request = [];
    protected array $dt_add_column = [];
    protected array $dt_edit_column = [];
    protected array $dt_remove_column = [];

    public function datatable(array $request)
    {
        $this->dt_request = array_replace($this->dt_request, $request);
        $this->processRequest();

        return $this;
    }

    protected function processRequest()
    {
        $column_list = $this->dt_request['columns'];
        $search_value = $this->dt_request['search']['value'];
        $this->dt_limit = $this->dt_request['length'];
        $this->dt_offset = $this->dt_request['start'];

        foreach ($column_list as $column) {
            if (!empty($column['name']) && !empty($column['data'])) {
                $this->dtSelect("{$column['name']} AS {$column['data']}");
            } else if (!empty($column['name']) && empty($column['data'])) {
                $this->dtSelect($column['name']);
            }
        }

        if (!empty($search_value)) {
            foreach ($column_list as $column) {
                if (!empty($column['searchable']) && ($column['searchable'] === 'true')) {
                    $this->dt_filter_request[] = "{$column['data']} LIKE '%{$this->db->escape_like_str($search_value)}%' ESCAPE '!'";
                }
            }
        }

        foreach ($this->dt_request['order'] as $order) {
            if (!empty($column_list[$order['column']]['data'])) {
                $this->dtOrderBy($column_list[$order['column']]['data'], $order['dir']);
            } else if (!empty($column_list[$order['column']]['name'])) {
                $this->dtOrderBy($column_list[$order['column']]['name'], $order['dir']);
            }
        }
    }

    public function dtSelect(string $select, bool $escape = NULL)
    {
        $this->dt_select[] = [$select, $escape];

        return $this;
    }

    public function dtFrom(string $table)
    {
        $this->dt_from = $table;

        return $this;
    }

    public function dtJoin(string $table, string $condition, string $type = '', bool $escape = NULL)
    {
        $this->dt_join[] = [$table, $condition, $type, $escape];

        return $this;
    }

    public function dtWhere($key, $value = NULL, bool $escape = NULL)
    {
        $this->dt_where[] = [$key, $value, $escape];

        return $this;
    }

    public function dtWhereIn($key, array $value = [], bool $escape = NULL)
    {
        $this->dt_where_in[] = [$key, $value, $escape];

        return $this;
    }

    public function dtWhereNotIn($key, array $value = [], bool $escape = NULL)
    {
        $this->dt_where_not_in[] = [$key, $value, $escape];

        return $this;
    }

    public function dtOrWhere($key, $value = NULL, bool $escape = NULL)
    {
        $this->dt_or_where[] = [$key, $value, $escape];

        return $this;
    }

    public function dtOrWhereIn($key, array $value = [], bool $escape = NULL)
    {
        $this->dt_or_where_in[] = [$key, $value, $escape];

        return $this;
    }

    public function dtOrWhereNotIn($key, array $value = [], bool $escape = NULL)
    {
        $this->dt_or_where_not_in[] = [$key, $value, $escape];

        return $this;
    }

    public function dtLike($field, $match = '', string $side = 'both', bool $escape = NULL)
    {
        $this->dt_like[] = [$field, $match, $side, $escape];

        return $this;
    }

    public function dtNotLike($field, $match = '', string $side = 'both', bool $escape = NULL)
    {
        $this->dt_not_like[] = [$field, $match, $side, $escape];

        return $this;
    }

    public function dtOrLike($field, $match = '', string $side = 'both', bool $escape = NULL)
    {
        $this->dt_or_like[] = [$field, $match, $side, $escape];

        return $this;
    }

    public function dtOrNotLike($field, $match = '', string $side = 'both', bool $escape = NULL)
    {
        $this->dt_or_not_like[] = [$field, $match, $side, $escape];

        return $this;
    }

    public function dtHaving($key, $value = NULL, bool $escape = NULL)
    {
        $this->dt_having[] = [$key, $value, $escape];

        return $this;
    }

    public function dtOrHaving($key, $value = NULL, bool $escape = NULL)
    {
        $this->dt_or_having[] = [$key, $value, $escape];

        return $this;
    }

    public function dtGroupBy($group_by, bool $escape = NULL)
    {
        $this->dt_group_by[] = [$group_by, $escape];

        return $this;
    }

    public function dtOrderBy($keys, $order = 'asc', bool $escape = NULL)
    {
        $this->dt_order_by[] = [$keys, $order, $escape];

        return $this;
    }

    public function dtPrependOrderBy($keys, $order = 'asc', bool $escape = NULL)
    {
        array_unshift($this->dt_order_by, [$keys, $order, $escape]);

        return $this;
    }

    public function dtAddColumn(string $column, string $content, $string_replacement = NULL)
    {
        $this->dt_add_column[$column] = [
            'content' => $content,
            'replacement' => (array) $string_replacement
        ];

        return $this;
    }

    public function dtEditColumn(string $column, string $content, $string_replacement)
    {
        $this->dt_add_column[$column] = [
            'content' => $content,
            'replacement' => (array) $string_replacement
        ];

        return $this;
    }

    public function dtRemoveColumn($column)
    {
        $this->dt_remove_column = array_merge($this->dt_remove_column, (array) $column);

        return $this;
    }

    public function dtGet(): DatatableResult
    {
        $record_total_query = $this->dtRecordsTotal();
        $record_filter_query = $this->dtRecordsFiltered();
        $data = $this->dtData();

        return new DatatableResult(
            $this->dt_request['draw'],
            $this->db->simple_query($record_total_query)->num_rows,
            $this->db->simple_query($record_filter_query)->num_rows,
            $data
        );
    }

    protected function dtRecordsTotal(): string
    {
        $this->buildDtQueryLoop([
            'select', 'join', 'where', 'where_in',
            'where_not_in', 'or_where', 'or_where_in', 'or_where_in',
            'or_where_not_in', 'like', 'not_like', 'or_like',
            'or_not_like', 'group_by', 'having', 'or_having'
        ])
            ->buildDtQueryFrom();

        return $this->db->get_compiled_select('', FALSE);
    }

    protected function dtRecordsFiltered(): string
    {
        $this->buildDtQueryFilter();

        return $this->db->get_compiled_select('', FALSE);
    }

    protected function dtData(): array
    {
        $this->buildDtQueryLoop(['order_by'])->buildDtQueryLimit();

        $result = $this->db->get()->result();

        $found_add_replace = ['index' => [], 'column' => []];
        $found_edit_replace = ['index' => [], 'column' => []];
        $result = array_map(function ($row) use ($found_add_replace, $found_edit_replace) {
            foreach ($this->dt_add_column as $column => $column_row) {
                $this->replaceContent($row, $column_row['content'], $column_row['replacement'], $found_add_replace, $column);
            }

            foreach ($this->dt_edit_column as $column => $column_row) {
                $this->replaceContent($row, $column_row['content'], $column_row['replacement'], $found_edit_replace, $column);
            }

            return $row;
        }, $result);

        return $result;
    }

    protected function replaceContent(object $row_data, string $content, array $replace_list, array &$found_replace, $column): string
    {
        if (!empty($replace_list)) {
            if (!isset($found_replace['index'][$column])) {
                $found_replace['index'][$column] = (bool) preg_match_all('/{{\$\d}}/', $content);
            }

            if ($found_replace['index'][$column]) {
                foreach ($replace_list as $key => $replace) {
                    $content = str_replace('{{$' . ($key + 1) . '}}', $row_data->$replace, $content);
                }
            }
        }

        if (!isset($found_replace['column'][$column])) {
            preg_match_all('/{{\$[a-z0-9_]+}}/i', $content, $found_replace['column'][$column]);
        }

        if (!empty($found_replace['column'][$column])) {
            foreach ($found_replace['column'][$column][0] as $key) {
                $key = preg_replace('/[{}$]+/', '', $key);
                $content = str_replace('{{$' . $key . '}}', $row_data->$key, $content);
            }
        }

        return $content;
    }

    protected function buildDtQueryLoop($property)
    {
        $list = (array) $property;

        foreach ($list as $method) {
            foreach ($this->{"dt_{$method}"} as $row) {
                $this->db->$method(...$row);
            }
        }

        return $this;
    }

    protected function buildDtQueryFrom()
    {
        $this->db->from($this->dt_from ?: $this->table);

        return $this;
    }

    protected function buildDtQueryFilter()
    {
        foreach ($this->dt_filter_request as $row) {
            $this->db->or_having($row);
        }

        return $this;
    }

    protected function buildDtQueryLimit()
    {
        if ($this->dt_limit > 0) {
            $this->db->limit($this->dt_limit, $this->dt_offset);
        }

        return $this;
    }
}
