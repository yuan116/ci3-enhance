<?php

namespace Yuan116\Ci3\Enhance\Libraries\Datatable;

defined('CI_VERSION') or die('Only for Codeigniter 3 (CI3)');

use Closure;

class DatatableResult
{
    protected array $dt_result = [
        'draw' => 0,
        'recordsTotal' => 0,
        'recordsFiltered' => 0,
        'data' => []
    ];

    public function __construct(int $draw, int $record_total, int $record_filtered, array $data)
    {
        $this->dt_result = [
            'draw' => $draw,
            'recordsTotal' => $record_total,
            'recordsFiltered' => $record_filtered,
            'data' => $data
        ];
    }

    public function map(Closure $callback)
    {
        $this->dt_result['data'] = array_map($callback, $this->dt_result['data']);

        return $this;
    }

    public function toArray(): array
    {
        return $this->dt_result;
    }

    public function toJson(): string
    {
        return json_encode($this->dt_result);
    }
}
