<?php

namespace Yuan116\Ci3\Enhance\Libraries\Password\Drivers;

defined('CI_VERSION') or die('Only for Codeigniter 3 (CI3)');

class Bcrypt extends AbstractPassword
{
    protected $algorithm = PASSWORD_BCRYPT;

    protected $option_list = [
        'cost' => PASSWORD_BCRYPT_DEFAULT_COST
    ];
}
