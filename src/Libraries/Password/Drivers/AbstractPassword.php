<?php

namespace Yuan116\Ci3\Enhance\Libraries\Password\Drivers;

defined('CI_VERSION') or die('Only for Codeigniter 3 (CI3)');

use UnexpectedValueException;

abstract class AbstractPassword
{
    public function getAlgorithm()
    {
        return $this->algorithm;
    }

    public function getOptions(array $option_list = []): array
    {
        $this->setOptions($option_list);

        return $this->option_list;
    }

    public function setOptions(array $option_list)
    {
        foreach ($option_list as $option => $value) {
            if (!in_array($option, $this->option_list)) {
                $class = get_class($this);
                throw new UnexpectedValueException("Trying to set option that not available in {$class} option");
            }

            $this->option_list[$option] = (int) $value;
        }

        return $this;
    }
}
