<?php

namespace Yuan116\Ci3\Enhance\Libraries\Password\Drivers;

defined('CI_VERSION') or die('Only for Codeigniter 3 (CI3)');

class Argon2i extends AbstractPassword
{
    protected $algorithm = PASSWORD_ARGON2I;

    protected $option_list = [
        'threads' => PASSWORD_ARGON2_DEFAULT_THREADS,
        'time_cost' => PASSWORD_ARGON2_DEFAULT_TIME_COST,
        'memory_cost' => PASSWORD_ARGON2_DEFAULT_MEMORY_COST
    ];
}
