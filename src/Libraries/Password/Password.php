<?php

namespace Yuan116\Ci3\Enhance\Libraries\Password;

defined('CI_VERSION') or die('Only for Codeigniter 3 (CI3)');

use BadMethodCallException;
use CompileError;

class Password
{
    public function __construct(array $config = [])
    {
        $this->setAlgorithm($config['algorithm'] ?? PASSWORD_DEFAULT);
        $this->setOptions($config['options'] ?? []);
    }

    public function __call($method, $arguments)
    {
        if (method_exists($this->driver, $method)) {
            return $this->driver->$method(...$arguments);
        }

        throw new BadMethodCallException("Call to undefined method Password::{$method}()");
    }

    public function setAlgorithm($algorithm): Password
    {
        switch ($algorithm) {
            case constant('PASSWORD_DEFAULT'):
            case constant('PASSWORD_BCRYPT'):
                $class = 'Bcrypt';
                break;
            case constant('PASSWORD_ARGON2I'):
                $class = 'Argon2i';
                break;
            case constant('PASSWORD_ARGON2ID'):
                $class = 'Argon2id';
                break;
            default:
                throw new CompileError("Use of undefined constant hashing algorithm : {$algorithm}");
                break;
        }

        $class = __NAMESPACE__ . '\\Drivers\\' . $class;

        $this->driver = new $class();

        return $this;
    }

    public function hash($password, array $options = [])
    {
        return password_hash(
            $password,
            $this->driver->getAlgorithm(),
            $this->driver->getOptions($options)
        );
    }

    public function verify($password, $hash): bool
    {
        return password_verify($password, $hash);
    }

    public function info($hash): array
    {
        return password_get_info($hash);
    }

    public function needRehash($hash, array $options = []): bool
    {
        return password_needs_rehash(
            $hash,
            $this->driver->getAlgorithm(),
            $this->driver->getOptions($options)
        );
    }
}
