<?php

namespace Yuan116\Ci3\Enhance\Libraries;

defined('CI_VERSION') or die('Only for Codeigniter 3 (CI3)');

class MY_Email extends \CI_Email
{
    public function __construct(array $config = [])
    {
        parent::__construct($config);
    }

    public function sendEmail(array $data): bool
    {
        foreach (array_keys($data) as $method_name) {
            if (is_array($data[$method_name])) {
                $this->$method_name(...$data[$method_name]);
            } else {
                $this->$method_name($data[$method_name]);
            }
        }

        return parent::send();
    }
}
