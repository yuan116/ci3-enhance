<?php

namespace Yuan116\Ci3\Enhance\Libraries;

defined('CI_VERSION') or die('Only for Codeigniter 3 (CI3)');

use League\Flysystem\Filesystem;
use League\Flysystem\Local\LocalFilesystemAdapter;
use RuntimeException;

class MY_Form_validation extends \CI_Form_validation
{
    protected string $root = APPPATH . 'rules' . DIRECTORY_SEPARATOR;
    protected array $rules_data = [];
    protected Filesystem $file_system;

    public function __construct()
    {
        parent::__construct();
        $this->file_system = new Filesystem(new LocalFilesystemAdapter($this->root));
    }

    public function setRulesData($key, $value = NULL)
    {
        if (is_array($key)) {
            $this->rules_data = array_merge($this->rules_data, $key);
        } else {
            $this->rules_data[$key] = $value;
        }

        return $this;
    }

    public function loadRules(string $class, string $method = '')
    {
        static $loaded_class = [];

        $class = str_ireplace('controller', '', $class) . 'Rule';

        if (!array_key_exists($class, $loaded_class)) {
            if (!$this->file_system->fileExists($class . '.php')) {
                throw new RuntimeException("Class '{$class}' not found");
            }

            require_once $this->root . $class . '.php';

            $loaded_class[$class] = new $class($this->rules_data);
        }

        $rule_class = $loaded_class[$class];

        if ($method === '') {
            $method = $this->CI->router->method;
        }

        if (method_exists($rule_class, $method)) {
            return $this->set_rules($rule_class->$method());
        }

        return $this->set_rules($rule_class->getRules());
    }

    /**
     * {@inheritDoc}
     */
    public function set_rules($field, $label = '', $rules = [], $errors = [])
    {
        // No reason to set rules if we have no POST data
        // or a validation array has not been specified
        if (!in_array($this->CI->input->method(), ['post', 'put', 'delete'], TRUE) && empty($this->validation_data)) {
            return $this;
        }

        // If an array was passed via the first parameter instead of individual string
        // values we cycle through it and recursively call this function.
        if (is_array($field)) {
            foreach ($field as $row) {
                // Houston, we have a problem...
                if (!isset($row['field'], $row['rules'])) {
                    continue;
                }

                // If the field label wasn't passed we use the field name
                $label = isset($row['label']) ? $row['label'] : $row['field'];

                // Add the custom error message array
                $errors = (isset($row['errors']) && is_array($row['errors'])) ? $row['errors'] : array();

                // Here we go!
                $this->set_rules($row['field'], $label, $row['rules'], $errors);
            }

            return $this;
        }

        // No fields or no rules? Nothing to do...
        if (!is_string($field) or $field === '' or empty($rules)) {
            return $this;
        } elseif (!is_array($rules)) {
            // BC: Convert pipe-separated rules string to an array
            if (!is_string($rules)) {
                return $this;
            }

            $rules = preg_split('/\|(?![^\[]*\])/', $rules);
        }

        // If the field label wasn't passed we use the field name
        $label = ($label === '') ? $field : $label;

        $indexes = array();

        // Is the field name an array? If it is an array, we break it apart
        // into its components so that we can fetch the corresponding POST data later
        if (($is_array = (bool) preg_match_all('/\[(.*?)\]/', $field, $matches)) === TRUE) {
            sscanf($field, '%[^[][', $indexes[0]);

            for ($i = 0, $c = count($matches[0]); $i < $c; $i++) {
                if ($matches[1][$i] !== '') {
                    $indexes[] = $matches[1][$i];
                }
            }
        }

        // Build our master array
        $this->_field_data[$field] = array(
            'field'     => $field,
            'label'     => $label,
            'rules'     => $rules,
            'errors'    => $errors,
            'is_array'  => $is_array,
            'keys'      => $indexes,
            'postdata'  => NULL,
            'error'     => ''
        );

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function run($group = '')
    {
        $class = str_ireplace('controller', '', $this->CI->router->class);

        if ($this->file_system->fileExists($class . 'Rule.php')) {
            $this->loadRules($class, $group);
        }

        return parent::run($group);
    }
}
