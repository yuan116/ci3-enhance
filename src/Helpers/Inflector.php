<?php

namespace Yuan116\Ci3\Enhance\Helpers;

defined('CI_VERSION') or die('Only for Codeigniter 3 (CI3)');

use Doctrine\Inflector\InflectorFactory;

class Inflector
{
    public static function __callStatic($name, $arguments)
    {
        static $inflector = NULL;

        if ($inflector === NULL) {
            $inflector = InflectorFactory::create()->build();
        }

        return $inflector->$name(...$arguments);
    }

    public static function abbrToUnderscore(string $str, array $remove_char_list = []): string
    {
        $remove_char_list = array_map('strtolower', $remove_char_list);

        $str_array = preg_split('/(^[^A-Z]+|[A-Z][^A-Z]+)/', $str, 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);

        if (!empty($remove_char_list)) {
            $str_array = array_filter($str_array, function ($value) use ($remove_char_list) {
                return !in_array(strtolower($value), $remove_char_list);
            });
        }

        return strtolower(implode('_', $str_array));
    }
}
