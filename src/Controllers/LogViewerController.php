<?php

namespace Yuan116\Ci3\Enhance\Controllers;

defined('CI_VERSION') or die('Only for Codeigniter 3 (CI3)');

use Yuan116\Ci3\Enhance\Core\MY_Controller;
use Yuan116\Ci3\Enhance\Libraries\Password\Password;

use RuntimeException;

class LogViewerController extends MY_Controller
{
    protected bool $auth = FALSE;
    protected int $auth_timeout = 600; //10 minutes
    public string $route_url = '';

    protected array $log_config = [
        'log_path' => APPPATH . 'logs' . DIRECTORY_SEPARATOR,
        'log_file_extension' => '.php'
    ];

    private const LOG_VIEWER_AUTH_TABLE = 'ci_log_viewer_auth';
    protected const LOG_LEVEL_PATTERN = '/ERROR|DEBUG|INFO|ALL/';
    protected const LOG_LEVEL_BADGE = [
        'ERROR' => 'danger',
        'DEBUG' => 'warning',
        'INFO' => 'info',
        'ALL' => 'primary'
    ];

    public function __construct()
    {
        parent::__construct();
        $this->preventDirectAccess();

        $this->load->library('session')
            ->database();
        $this->createTable();

        foreach ($this->log_config as $key => $value) {
            $config_value = $this->config->item($key);
            $this->log_config[$key] = empty($config_value) ? $value : $config_value;
        }
    }

    protected function preventDirectAccess(): void
    {
        if ($this->route_url === '') {
            $class = get_class($this);
            throw new RuntimeException("{$class}::\$route_url cannot be empty string");
        }

        if (strpos(strtolower($this->route_url), strtolower($this->uri->uri_string())) === FALSE) {
            show_404($this->uri->uri_string());
        }
    }

    protected function createTable(): void
    {
        if (!$this->db->table_exists(self::LOG_VIEWER_AUTH_TABLE)) {
            $this->load->dbforge();

            $this->dbforge->add_field([
                'id' => [
                    'type' => 'INT',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE
                ],
                'username' => [
                    'type' => 'VARCHAR',
                    'constraint' => '100'
                ],
                'password' => [
                    'type' => 'VARCHAR',
                    'constraint' => '255'
                ],
                'last_login' => [
                    'type' => 'DATETIME',
                    'null' => TRUE
                ],
                'datetime' => [
                    'type' => 'DATETIME'
                ]
            ])
                ->add_key('id', TRUE)
                ->create_table(self::LOG_VIEWER_AUTH_TABLE);
        }
    }

    public function index()
    {
        $this->load->add_package_path(dirname(__DIR__));

        $found = $this->db->get_where(self::LOG_VIEWER_AUTH_TABLE, ['id' => $this->session->log_viewer_auth_id])->data_seek();

        if ($this->auth && !$found) {
            $this->auth();
            $this->load->view('log/log_viewer_auth');
        } else {
            $data['filename'] = $this->input->get('filename');
            $data['log_list'] = $this->getFileList();

            $file = "{$this->log_config['log_path']}log-{$data['filename']}{$this->log_config['log_file_extension']}";
            $data['content_list'] = [];
            if (is_file($file)) {
                $content_list = file($file, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

                $data['content_list'] = $this->getLogContent($content_list);
            }

            $data['auth_timeout'] = $this->getAuthRemainingTime();
            $this->load->view('log/log_viewer_list', $data);
        }
    }

    protected function auth()
    {
        if (!isset($_POST['auth_login_btn'])) {
            return;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('username', 'Username', 'trim|required')
            ->set_rules('password', 'Password', 'trim|required')
            ->set_rules('auth', '', [[
                'auth_check_callback', function ($value) {
                    $post = $this->input->post(['username', 'password'], TRUE);

                    $auth = $this->db->get_where(self::LOG_VIEWER_AUTH_TABLE, ['username' => $post['username']])->row();
                    if (!empty($auth)) {
                        $password = new Password();
                        if ($password->verify($post['password'], $auth->password)) {
                            if ($password->needRehash($auth->password)) {
                                $this->db->update(
                                    self::LOG_VIEWER_AUTH_TABLE,
                                    ['password' => $password->hash($post['password'])],
                                    ['id' => $auth->id]
                                );
                            }

                            return TRUE;
                        }
                    }

                    return FALSE;
                }
            ]], ['auth_check_callback' => 'Username or Password is incorrect.']);

        if ($this->form_validation->run()) {
            $username = $this->input->post('username');
            $auth = $this->db->get_where(self::LOG_VIEWER_AUTH_TABLE, ['username' => $username])->row();
            $this->db->update(self::LOG_VIEWER_AUTH_TABLE, ['last_login' => date('Y-m-d H:i:s')], ['id' => $auth->id]);

            $this->session->set_tempdata('log_viewer_auth_id', $auth->id, $this->auth_timeout);
            redirect($this->route_url);
        }
    }

    protected function getFileList(): array
    {
        $log_file_extension = $this->log_config['log_file_extension'];
        $file_list = (glob("{$this->log_config['log_path']}log-*{$log_file_extension}"));

        $file_list = array_map(function ($value) use ($log_file_extension) {
            return preg_replace("/log-|{$log_file_extension}/", '', basename($value));
        }, $file_list);

        rsort($file_list);

        return $file_list;
    }

    protected function getLogContent(array $content_list): array
    {
        $final_content_list = [];

        $count = 0;
        foreach ($content_list as $content) {
            if ($this->isStartLogLevel($content, $log_level)) {
                $final_content_list[] = [
                    'badge' => self::LOG_LEVEL_BADGE[$log_level],
                    'level' => $log_level,
                    'datetime' => trim(str_replace("{$log_level} - ", '', strstr($content, '-->', TRUE))),
                    'content' => trim(substr($content, strpos($content, '-->') + 3))
                ];
                $count++;
            } else if (!empty($final_content_list)) {
                $final_content_list[$count - 1]['content'] = $final_content_list[$count - 1]['content'] . PHP_EOL . $content;
            }
        }

        return $final_content_list;
    }

    protected function isStartLogLevel(string $line_content, &$log_level): bool
    {
        if (preg_match(self::LOG_LEVEL_PATTERN, $line_content, $match_list, PREG_OFFSET_CAPTURE)) {
            //Log level string must be start from 0
            if ($match_list[0][1] === 0) {
                $log_level = $match_list[0][0];
                return TRUE;
            }

            return FALSE;
        }

        return FALSE;
    }

    protected function getAuthRemainingTime(): int
    {
        $current_time = time();
        $log_viewer_time = $_SESSION['__ci_vars']['log_viewer_auth_id'] ?? $current_time;

        return $log_viewer_time - $current_time;
    }
}
