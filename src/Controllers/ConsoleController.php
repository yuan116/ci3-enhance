<?php

namespace Yuan116\Ci3\Enhance\Controllers;

defined('CI_VERSION') or die('Only for Codeigniter 3 (CI3)');

use League\Flysystem\{
    Filesystem,
    StorageAttributes
};
use League\Flysystem\Local\LocalFilesystemAdapter;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Command\Command;
use Yuan116\Ci3\Enhance\Core\MY_Controller;

class ConsoleController extends MY_Controller
{
    protected Application $app;
    protected Filesystem $ci_app_file_system;
    protected Filesystem $vendor_file_system;

    public function __construct()
    {
        parent::__construct();
        $this->app = new Application('Codeigniter 3 Enhance', CI_VERSION);
        $this->ci_app_file_system = new Filesystem(new LocalFilesystemAdapter(APPPATH));
        $this->vendor_file_system = new Filesystem(new LocalFilesystemAdapter(dirname(__DIR__)));
    }

    public function index()
    {
        $this->getVendorConsoleList()->getAppConsoleList();
        $this->app->run();
    }

    protected function getVendorConsoleList()
    {
        $file_list = $this->vendor_file_system->listContents('Consoles', TRUE)
            ->filter(function (StorageAttributes $attributes) {
                return $attributes->isFile();
            })
            ->map(function (StorageAttributes $attributes) {
                return pathinfo($attributes->path());
            })
            ->toArray();

        foreach ($file_list as $file) {
            if (strpos($file['filename'], 'Trait') === FALSE) {
                if ($file['extension'] === 'php') {
                    $namespaced_class = '\Yuan116\Ci3\Enhance';
                    if (!empty($file['dirname'])) {
                        $namespaced_class .= '\\' . preg_replace('/(\\/)/', '\\', ucfirst($file['dirname']));
                    }

                    $namespaced_class .= '\\' . $file['filename'];
                    $object = new $namespaced_class();

                    if ($object instanceof Command) {
                        $this->app->add($object);
                    }

                    unset($object);
                }
            }
        }

        return $this;
    }

    protected function getAppConsoleList()
    {
        $file_list = $this->ci_app_file_system->listContents('consoles', TRUE)
            ->filter(function (StorageAttributes $attributes) {
                return $attributes->isFile();
            })
            ->map(function (StorageAttributes $attributes) {
                return pathinfo($attributes->path());
            })
            ->toArray();

        foreach ($file_list as $file) {
            if (strpos($file['filename'], 'Trait') === FALSE) {
                if ($file['extension'] === 'php') {
                    require_once APPPATH . $file['dirname'] . DIRECTORY_SEPARATOR . $file['basename'];
                    $object = new $file['filename']();

                    if ($object instanceof Command) {
                        $this->app->add($object);
                    }

                    unset($object);
                }
            }
        }

        return $this;
    }
}
