<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Codeigniter 3 Enhance Framework - Log Viewer</title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.datatables.net/v/bs4/dt-1.10.23/r-2.2.7/datatables.min.css" />
</head>

<body>
    <div class="container-fluid px-5">
        <div class="row my-3">
            <div class="col-lg-12">
                <span class="h2">Codeigniter 3 Enhance Framework - Log Viewer</span>
                <span class="float-right h5 mt-2 mb-0" id="session-timeout"></span>
            </div>
        </div>

        <div class="row mb-5">
            <div class="col-xl-2 pb-3">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            <h5>Log File List</h5>
                        </div>
                    </div>
                    <div class="card-body" style="max-height: 450px;">
                        <div class="form-group">
                            <input type="text" class="form-control" name="search_log_input" id="search_log_input" placeholder="Search Log" />
                        </div>
                        <div class="list-group overflow-auto" id="search_log_container" style="max-height: 300px;">
                            <?php if (empty($log_list)) : ?>
                                <a href="#" class="list-group-item active">No Log File Found</a>
                            <?php else : ?>
                                <?php foreach ($log_list as $log) : ?>
                                    <a href="<?php echo site_url($this->route_url . '?filename=' . $log); ?>" class="list-group-item <?php echo $log === $filename ? 'active' : ''; ?>">
                                        <?php echo $log; ?>
                                    </a>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-10">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            <h5>Log Content List</h5>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped table-sm" id="tbl_log">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Level</th>
                                        <th>Datetime</th>
                                        <th>Content</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($content_list as $content) : ?>
                                        <tr>
                                            <td></td>
                                            <td><span class="badge badge-<?php echo $content['badge']; ?>"><?php echo $content['level']; ?></span></td>
                                            <td><?php echo $content['datetime']; ?></td>
                                            <td><?php echo nl2br($content['content']); ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer class="pt-5 d-sm-none d-md-block">
        <div class="fixed-bottom bg-primary p-3">
            <b>Codeigniter 3 Enhance Framework - Log Viewer</b>
        </div>
    </footer>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/5a4811c790.js" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/v/bs4/dt-1.10.23/r-2.2.7/datatables.min.js"></script>
    <script>
        $(function() {
            <?php if ($auth_timeout > 0) : ?>
                let session_timeout = <?php echo $auth_timeout; ?>;
                $('#session-timeout').html(`Session Timeout : ${session_timeout}s`);

                setInterval(async () => {
                    session_timeout--;
                    $('#session-timeout').html(`Session Timeout : ${session_timeout}s`);

                    if (session_timeout === 0) {
                        await window.alert('Session Timeout!');
                        window.location.reload();
                    }
                }, 1000);
            <?php endif; ?>

            $('#search_log_input').keyup(function() {
                let value = $(this).val();
                $("#search_log_container a").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
                });
            });

            $('#tbl_log').DataTable({
                processing: false,
                serverSide: false,
                order: [
                    [1, 'asc']
                ],
                autoWidth: false,
                info: true,
                lengthChange: true,
                ordering: true,
                paging: true,
                searching: true,
                responsive: true,
                columnDefs: [{
                    targets: 0,
                    orderable: false,
                    searchable: false,
                    width: '10px'
                }],
                drawCallback: function(settings) {
                    let datatable_api = this.api();

                    datatable_api.column(0, {
                        search: 'applied',
                        order: 'applied'
                    }).nodes().each(function(cell, i) {
                        cell.innerHTML = i + 1
                    });
                }
            });
        });
    </script>
</body>

</html>