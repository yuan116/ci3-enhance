<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Codeigniter 3 Enhance Framework - Log Viewer</title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
</head>

<body>
    <div class="container px-5 pt-5 mt-5">
        <div class="row mt-5">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <h4 class="text-center">Codeigniter 3 Enhance Framework <br /> Log Viewer</h4>
                <div class="card">
                    <div class="card-body">
                        <?php $auth_error = form_error('auth'); ?>
                        <?php if (!empty($auth_error)) : ?>
                            <div class="alert alert-danger">
                                <?php echo $auth_error; ?>
                            </div>
                        <?php endif; ?>
                        <form action="<?php echo current_url(); ?>" method="post">
                            <div class="form-group">
                                <label for="username" class="control-label">Username</label>
                                <input type="text" class="form-control" name="username" id="username" required />
                            </div>

                            <div class="form-group">
                                <label for="password" class="control-label">Password</label>
                                <input type="password" class="form-control" name="password" id="password" required />
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary float-right" name="auth_login_btn" value="auth_login">Login</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer class="pt-5 d-md-none d-lg-block">
        <div class="fixed-bottom bg-primary p-3">
            <b>Codeigniter 3 Enhance Framework - Log Viewer</b>
        </div>
    </footer>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</body>

</html>