<?php

namespace Yuan116\Ci3\Enhance\Consoles\Environments;

defined('CI_VERSION') or die('Only for Codeigniter 3 (CI3)');

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\{
    InputArgument,
    InputInterface
};
use Symfony\Component\Console\Output\OutputInterface;

class Environment extends Command
{
    public function __construct()
    {
        parent::__construct('env');
    }

    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this->setDescription('Change application environment')
            ->addArgument(
                'type',
                InputArgument::REQUIRED,
                'Type of environment (dev, test, prod).'
            );
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $type = $input->getArgument('type');

        switch ($type) {
            case 'dev':
            case 'development':
                $type = 'development';
                break;
            case 'test':
            case 'testing':
            case 'prod':
            case 'production':
                $type = 'production';
                break;
            default:
                $output->writeln('<error>No such environment configured in default CI project.</error>');
                exit();
                break;
        }

        $env_string = "define('ENVIRONMENT', '{$type}');";

        $index_content = file_get_contents(FCPATH . 'index.php');
        $env_start_string = strpos($index_content, 'define(');
        $env_end_string = strpos($index_content, ');', $env_start_string) + 2;

        file_put_contents('index.php', substr_replace($index_content, $env_string, $env_start_string, $env_end_string - $env_start_string));

        return Command::SUCCESS;
    }
}
