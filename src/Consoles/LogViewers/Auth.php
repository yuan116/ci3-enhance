<?php

namespace Yuan116\Ci3\Enhance\Consoles\LogViewers;

defined('CI_VERSION') or die('Only for Codeigniter 3 (CI3)');

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\{
    InputInterface
};
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class Auth extends Command
{
    use LogViewerHelperTrait;

    public function __construct()
    {
        parent::__construct('log-viewer:auth');
        $this->setPasswordInstance();
    }

    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this->setDescription('Create username and password for log in Log Viewer');
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $ci = get_instance();
        $ci->load->database();
        $this->createTable($ci);
        $db = $ci->db;

        $helper = $this->getHelper('question');
        $question_username = new Question('Please enter username : ');
        $question_username->setValidator(function ($answer) use ($db) {
            $this->validateRequired('Username', $answer);
            $this->validateUsername($db, $answer, TRUE);

            return $answer;
        });
        $answer_username = $helper->ask($input, $output, $question_username);

        $question_password = new Question('Please enter password : ');
        $question_password->setValidator(function ($answer) {
            $this->validateRequired('Password', $answer);

            return $answer;
        });
        $answer_password = $helper->ask($input, $output, $question_password);

        $db->insert($this->table, [
            'username' => $answer_username,
            'password' => $this->password->hash($answer_password),
            'datetime' => date('Y-m-d H:i:s')
        ]);

        $output->writeln('<info>User created successfully.</info>');

        return Command::SUCCESS;
    }
}
