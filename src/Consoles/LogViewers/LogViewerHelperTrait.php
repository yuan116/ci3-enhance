<?php

namespace Yuan116\Ci3\Enhance\Consoles\LogViewers;

use CI_Controller;
use Yuan116\Ci3\Enhance\Libraries\Password\Password;

use RuntimeException;

trait LogViewerHelperTrait
{
    protected string $table = 'ci_log_viewer_auth';
    protected Password $password;

    protected function createTable(CI_Controller $ci)
    {
        if (!$ci->db->table_exists($this->table)) {
            $ci->load->dbforge();

            $ci->dbforge->add_field([
                'id' => [
                    'type' => 'INT',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE
                ],
                'username' => [
                    'type' => 'VARCHAR',
                    'constraint' => '100'
                ],
                'password' => [
                    'type' => 'VARCHAR',
                    'constraint' => '255'
                ],
                'last_login' => [
                    'type' => 'DATETIME',
                    'null' => TRUE
                ],
                'datetime' => [
                    'type' => 'DATETIME'
                ]
            ])
                ->add_key('id', TRUE)
                ->create_table($this->table);
        }
    }

    protected function setPasswordInstance()
    {
        $this->password = new Password();
    }

    protected function validateRequired(string $field, $value): void
    {
        if (trim($value) === '') {
            throw new RuntimeException($field . ' is required');
        }
    }

    protected function validateUsername($db, $value, bool $check_exist)
    {
        if ($db->get_where($this->table, ['username' => $value])->data_seek() === $check_exist) {
            if ($check_exist) {
                throw new RuntimeException('Username exist. Please try another.');
            } else {
                throw new RuntimeException('Username does not exist. Please try again.');
            }
        }
    }
}
