<?php

namespace Yuan116\Ci3\Enhance\Consoles\LogViewers;

defined('CI_VERSION') or die('Only for Codeigniter 3 (CI3)');

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\{
    InputInterface
};
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\{
    ChoiceQuestion,
    Question
};

use RuntimeException;
use UnexpectedValueException;

class Change extends Command
{
    use LogViewerHelperTrait;

    protected const CHANGE_USERNAME = 1;
    protected const CHANGE_PASSWORD = 2;
    protected const CHANGE_LIST = [
        self::CHANGE_USERNAME => 'username',
        self::CHANGE_PASSWORD => 'password',
    ];

    public function __construct()
    {
        parent::__construct('log-viewer:change');
        $this->setPasswordInstance();
    }

    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this->setDescription('Change username or password for log in Log Viewer');
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $ci = get_instance();
        $ci->load->database();
        $this->createTable($ci);
        $db = $ci->db;
        $helper = $this->getHelper('question');
        $auth_id = $this->login($db, $helper, $input, $output);

        $question_change = new ChoiceQuestion('Select option : ', self::CHANGE_LIST);
        $answer_change = $helper->ask($input, $output, $question_change);

        switch (array_search($answer_change, self::CHANGE_LIST)) {
            case self::CHANGE_USERNAME:
                $this->changeUsername($db, $helper, $auth_id, $input, $output);
                break;
            case self::CHANGE_PASSWORD:
                $this->changePassword($db, $helper, $auth_id, $input, $output);
                break;
            default:
                throw new UnexpectedValueException('Please select' . implode(', ', self::CHANGE_LIST));
                break;
        }

        $output->writeln('<info>Save Successfully</info>');

        return Command::SUCCESS;
    }

    protected function login($db, $helper, InputInterface $input, OutputInterface $output)
    {
        $question_username = new Question('Please enter username : ');
        $question_username->setMaxAttempts(3);
        $question_username->setValidator(function ($answer) use ($db) {
            $this->validateRequired('Username', $answer);
            $this->validateUsername($db, $answer, FALSE);

            return $answer;
        });
        $answer_username = $helper->ask($input, $output, $question_username);

        $auth_data = $db->get_where($this->table, ['username' => $answer_username])->row();

        $question_password = new Question('Please enter password : ');
        $question_password->setMaxAttempts(3);
        $question_password->setValidator(function ($answer) use ($auth_data) {
            $this->validateRequired('Password', $answer);

            if (!$this->password->verify($answer, $auth_data->password)) {
                throw new RuntimeException('Password is incorrect');
            }

            return $answer;
        });

        $helper->ask($input, $output, $question_password);

        return $auth_data->id;
    }

    protected function changeUsername($db, $helper, $auth_id, InputInterface $input, OutputInterface $output)
    {
        $question_username = new Question('Please enter new username : ');
        $question_username->setValidator(function ($answer) use ($db) {
            $this->validateRequired('Username', $answer);
            $this->validateUsername($db, $answer, TRUE);

            return $answer;
        });
        $answer_username = $helper->ask($input, $output, $question_username);

        $db->update($this->table, ['username' => $answer_username], ['id' => $auth_id]);
    }

    protected function changePassword($db, $helper, $auth_id, InputInterface $input, OutputInterface $output)
    {
        $question_password = new Question('Please enter new password : ');
        $question_password->setValidator(function ($answer) {
            $this->validateRequired('Password', $answer);

            return $answer;
        });

        $answer_password = $helper->ask($input, $output, $question_password);

        $db->update($this->table, ['password' => $this->password->hash($answer_password)], ['id' => $auth_id]);
    }
}
