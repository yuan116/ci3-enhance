<?php

namespace Yuan116\Ci3\Enhance\Consoles\Seeders;

defined('CI_VERSION') or die('Only for Codeigniter 3 (CI3)');

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\{
    InputArgument,
    InputInterface
};
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Yuan116\Ci3\Enhance\Consoles\ConsoleHelperTrait;

class Table extends Command
{
    use ConsoleHelperTrait;

    protected $seeder;

    public function __construct()
    {
        parent::__construct('seed:table');
        $this->stub_file = 'seeder';
        $this->suffix = 'Seeder';
        $this->default_ci_subclass_prefix = '';
        $this->setSeederInstance()->setAppFileSystemInstance()->setVendorFileSystemInstance();
        $this->path = basename($this->seeder->getSeederPath());
    }

    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this->setDescription('Seed a table data into a seeder')
            ->addArgument(
                'name',
                InputArgument::REQUIRED,
                'Table name to seed.'
            );
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = $input->getArgument('name');

        $ci = get_instance();
        $ci->load->database();
        $db = $ci->db;

        if (!$db->table_exists($name)) {
            $output->writeln("<error>Table '{$name}' not exist.</error>");

            return Command::FAILURE;
        }

        $column_list = $ci->db->list_fields($name);

        $helper = $this->getHelper('question');
        $question_column_include = new ChoiceQuestion(
            'Which column(s) to include in seeder (Eg: 0, 1, 3, etc.., Default all will be include): ',
            $column_list,
            implode(',', $column_list)
        );
        $question_column_include->setMultiselect(TRUE)
            ->setErrorMessage('Column \'%s\' not exist.');

        $answer_column_include = $helper->ask($input, $output, $question_column_include);
        $output->writeln('Column(s) included: ' . implode(', ', $answer_column_include));

        if (count($column_list) !== count($answer_column_include)) {
            $column_list = array_intersect($column_list, $answer_column_include);
        } else {
            $question_column_exclude = new ChoiceQuestion(
                'Which column(s) to exclude in seeder (Eg: 0, 1, 3, etc.., Default all will be include): ',
                $column_list,
                implode(',', $column_list)
            );
            $question_column_exclude->setMultiselect(TRUE)
                ->setErrorMessage('Column \'%s\' not exist.');

            $answer_column_exclude = $helper->ask($input, $output, $question_column_exclude);
            if (count($column_list) !== count($answer_column_exclude)) {
                $column_list = array_diff($column_list, $answer_column_exclude);
            }
            $output->writeln('Column(s) included: ' . implode(', ', $column_list));
        }

        $result = $ci->db->select(implode(',', $column_list))->get($name)->result();

        return $this->seed($name, $column_list, $result, $output);
    }

    protected function seed(string $table, array $column_list, array $result, OutputInterface $output): int
    {
        $output->writeln("<comment>Start Seeding {$table}.</comment>");

        $name = $this->generateClassName($table);
        $stub_data[$this->stub_search_class] = $name;
        $stub_data[$this->stub_search_parent_class] = $this->checkParentFile('libraries', FALSE);
        $stub_data[$this->stub_search_seed_table] = $table;
        $stub_data[$this->stub_search_seed_query] = $this->generateSeedQuery($column_list, $result, $output);

        if ($this->app_file_system->fileExists($exist_file = $this->path . DIRECTORY_SEPARATOR . "{$name}.php")) {
            $this->app_file_system->move($exist_file, $this->path . DIRECTORY_SEPARATOR . 'backup' . DIRECTORY_SEPARATOR . date('Ymd_His') . "_{$name}.php");
        }

        return $this->helperGenerate($name, $output, $stub_data);
    }

    protected function generateSeedQuery(array $column_list, array $result, OutputInterface $output): string
    {
        $insert_start_string = "\t\t\$this->db->insert(\$this->table, [" . PHP_EOL;
        $insert_end_string = "\t\t]);" . PHP_EOL . PHP_EOL;

        $progress_bar = new ProgressBar($output, count($result));
        $progress_bar->setFormat('%current%/%max% [%bar%] %percent:3s%% (elapsed: %elapsed:6s% / estimated: %estimated:-6s%)');
        $progress_bar->start();

        $string = '';
        foreach ($result as $row) {
            $column_value = '';
            foreach ($column_list as $column) {
                $column_value .= "\t\t\t'{$column}' => stripslashes('" . addslashes(trim($row->$column)) . '\'),' . PHP_EOL;
            }

            $string = $string . $insert_start_string . $column_value . $insert_end_string;
            $progress_bar->advance();
        }

        $progress_bar->finish();

        echo PHP_EOL . PHP_EOL;

        return trim($string);
    }
}
