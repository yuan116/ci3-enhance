<?php

namespace Yuan116\Ci3\Enhance\Consoles\Seeders;

defined('CI_VERSION') or die('Only for Codeigniter 3 (CI3)');

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Yuan116\Ci3\Enhance\Consoles\ConsoleHelperTrait;

class File extends Command
{
    use ConsoleHelperTrait;

    public function __construct()
    {
        parent::__construct('seed:file');
        $this->setSeederInstance();
        $this->path = basename($this->seeder->getSeederPath());
    }

    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this->setDescription('Show seeder files list');
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $seeder_file_list = glob(APPPATH . $this->path . DIRECTORY_SEPARATOR . '*Seeder.php');

        if (empty($seeder_file_list)) {
            $formatter = $this->getHelper('formatter');
            $output->writeln($formatter->formatBlock('No seeder file found.', 'comment'));
        } else {
            $table = new Table($output);
            $table->setHeaders(['No.', 'Seeder Class Name']);

            $count = 1;
            $table_body_list = [];
            foreach ($seeder_file_list as $file_path) {
                $class_name = basename($file_path, '.php');

                $table_body_list[] = [
                    $count++,
                    $class_name
                ];
            }
            $table->setRows($table_body_list);
            $table->render();
        }

        return Command::SUCCESS;
    }
}
