<?php

namespace Yuan116\Ci3\Enhance\Consoles\Seeders;

defined('CI_VERSION') or die('Only for Codeigniter 3 (CI3)');

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Yuan116\Ci3\Enhance\Consoles\ConsoleHelperTrait;

class Generate extends Command
{
    use ConsoleHelperTrait;

    public function __construct()
    {
        parent::__construct('seed:generate');
        $this->stub_file = 'DatabaseSeeder';
        $this->suffix = 'Seeder';
        $this->default_ci_subclass_prefix = '';
        $this->setSeederInstance()
            ->setAppFileSystemInstance()
            ->setVendorFileSystemInstance();
        $this->path = basename($this->seeder->getSeederPath());
    }

    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this->setDescription('Generate database seeder into one file.');
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("<comment>Generating Database Seeder.</comment>");

        $seeder_file_list = glob(APPPATH . $this->path . DIRECTORY_SEPARATOR . '*Seeder.php');

        $name = $this->stub_file;
        $stub_data[$this->stub_search_parent_class] = $this->checkParentFile('libraries', FALSE);
        $stub_data[$this->stub_search_seed_classes] = $this->generateSeedClass($seeder_file_list);

        if ($this->app_file_system->fileExists($exist_file = $this->path . DIRECTORY_SEPARATOR . "{$name}.php")) {
            $this->app_file_system->move($exist_file, $this->path . DIRECTORY_SEPARATOR . 'backup' . DIRECTORY_SEPARATOR . date('Ymd_His') . "_{$name}.php");
        }

        return $this->helperGenerate($name, $output, $stub_data);
    }

    protected function generateSeedClass(array $seeder_file_list): string
    {
        $insert_start_string = "\t\t\t";
        $insert_end_string = "::class," . PHP_EOL;

        $string = '';
        foreach ($seeder_file_list as $file_path) {
            $class = basename($file_path, '.php');

            if ($class !== $this->stub_file) {
                $string .= $insert_start_string . $class . $insert_end_string;
            }
        }

        return trim($string);
    }
}
