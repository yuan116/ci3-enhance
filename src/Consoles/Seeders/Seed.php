<?php

namespace Yuan116\Ci3\Enhance\Consoles\Seeders;

defined('CI_VERSION') or die('Only for Codeigniter 3 (CI3)');

use RuntimeException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\{
    InputInterface,
    InputOption
};
use Symfony\Component\Console\Output\OutputInterface;
use Yuan116\Ci3\Enhance\Consoles\ConsoleHelperTrait;
use Yuan116\Ci3\Enhance\Helpers\Inflector;

class Seed extends Command
{
    use ConsoleHelperTrait;

    public function __construct()
    {
        parent::__construct('seed');
        $this->setSeederInstance()->setAppFileSystemInstance();
        $this->path = basename($this->seeder->getSeederPath());
    }

    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this->setDescription('Run seeder')
            ->addOption(
                'class',
                NULL,
                InputOption::VALUE_OPTIONAL,
                'Seeder class name to run. (Without Seeder suffix)',
                NULL
            );
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $option_input = $input->getOption('class');

        if ($option_input) {
            $seeder_file_list = glob(APPPATH . $this->path . DIRECTORY_SEPARATOR . Inflector::classify(str_ireplace('seeder', '', $option_input)) . 'Seeder.php');

            if (empty($seeder_file_list)) {
                $formatter = $this->getHelper('formatter');
                $output->writeln($formatter->formatBlock('Seeder not found.', 'error'));

                return Command::FAILURE;
            }
        } else {
            $formatter = $this->getHelper('formatter');

            if (!$this->app_file_system->fileExists($this->path . DIRECTORY_SEPARATOR . 'DatabaseSeeder.php')) {
                $output->writeln($formatter->formatBlock('DatabaseSeeder not found.', 'error'));
                $output->writeln($formatter->formatBlock('Please run command "php index.php seed:generate"', 'comment'));

                return Command::FAILURE;
            }

            if (!class_exists('DatabaseSeeder')) {
                $output->writeln($formatter->formatBlock('Please run command "composer dump-autoload"', 'comment'));

                return Command::FAILURE;
            }

            $seeder = new \DatabaseSeeder();
            $seeder->run();
            $seeder_file_list = $seeder->getClassList();
        }

        return $this->seed($seeder_file_list, $output);
    }

    protected function seed(array $seeder_file_list, OutputInterface $output): int
    {
        $total_time = 0;

        $formatter = $this->getHelper('formatter');

        try {
            foreach ($seeder_file_list as $file_path) {
                $class = basename($file_path, '.php');

                $output->writeln("<comment>Seeding</comment>: {$class} ...");
                $start = microtime(true);

                call_user_func([new $class, 'run']);

                $end = microtime(true) - $start;
                $total_time += $end;
                $output->writeln("<info>Seeded</info>: {$class} ." . $this->calculateTimeUsage($end));
            }
        } catch (RuntimeException $exeption) {
            $output->writeln($formatter->formatBlock($exeption->getMessage(), 'error'));
            $output->writeln($formatter->formatBlock('<comment>Please run command "composer dump-autoload" then run command "php index.php seed"', 'comment'));
        } finally {
            $output->writeln($formatter->formatBlock('Seeder run completed. Total ' . $this->calculateTimeUsage($total_time), 'info'));

            return Command::SUCCESS;
        }
    }
}
