<?php

namespace Yuan116\Ci3\Enhance\Consoles\Makers;

defined('CI_VERSION') or die('Only for Codeigniter 3 (CI3)');

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\{
    InputArgument,
    InputInterface
};
use Symfony\Component\Console\Output\OutputInterface;
use Yuan116\Ci3\Enhance\Consoles\ConsoleHelperTrait;

class Rule extends Command
{
    use ConsoleHelperTrait;

    public function __construct()
    {
        parent::__construct('make:rule');
        $this->path = 'rules';
        $this->stub_file = 'rule';
        $this->suffix = 'Rule';
        $this->setAppFileSystemInstance()->setVendorFileSystemInstance();
    }

    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this->setDescription('Create a new Rule')
            ->addArgument(
                'name',
                InputArgument::REQUIRED,
                "{$this->suffix} class name, by default will add suffix '{$this->suffix}' at class name."
            );
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = str_ireplace($this->suffix, '', $input->getArgument('name'));

        return $this->make($this->generateClassName($name), $output);
    }

    protected function make($name, OutputInterface $output): int
    {
        $stub_data[$this->stub_search_class] = $name;
        $stub_data[$this->stub_search_parent_class] = $this->checkParentFile('libraries', FALSE);

        return $this->helperGenerate($name, $output, $stub_data);
    }
}
