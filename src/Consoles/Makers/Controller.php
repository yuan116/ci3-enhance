<?php

namespace Yuan116\Ci3\Enhance\Consoles\Makers;

defined('CI_VERSION') or die('Only for Codeigniter 3 (CI3)');

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\{
    InputArgument,
    ArrayInput,
    InputInterface,
    InputOption
};
use Symfony\Component\Console\Output\OutputInterface;
use Yuan116\Ci3\Enhance\Consoles\ConsoleHelperTrait;
use Yuan116\Ci3\Enhance\Helpers\Inflector;

class Controller extends Command
{
    use ConsoleHelperTrait;

    protected $option_list = [
        'model' => 'make:model',
        'rule'  => 'make:rule'
    ];

    public function __construct()
    {
        parent::__construct('make:controller');
        $this->path = 'controllers';
        $this->stub_file = 'controller';
        $this->suffix = 'Controller';
        $this->setAppFileSystemInstance()->setVendorFileSystemInstance();
    }

    protected function getOptionStubList(): array
    {
        return [
            'model' => $this->stub_search_model,
            'rule' => $this->stub_search_rule
        ];
    }

    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this->setDescription('Create a new Controller')
            ->addArgument(
                'name',
                InputArgument::REQUIRED,
                "{$this->suffix} class name, by default will add suffix '{$this->suffix}' at class name."
            )
            ->addOption(
                'model',
                NULL,
                InputOption::VALUE_OPTIONAL,
                'Model class name, by default will add suffix "Model" at class name. 
                If no value apply will auto set Model class name same as Controller class name',
                FALSE
            )
            ->addOption(
                'rule',
                NULL,
                InputOption::VALUE_OPTIONAL,
                'Rule class name, by default will add suffix "Rule" at class name. 
                If no value apply will auto set Rule class name same as Controller class',
                FALSE
            );
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = str_ireplace($this->suffix, '', $input->getArgument('name'));

        $option_data = [];
        $option_stub_list = $this->getOptionStubList();
        foreach ($this->option_list as $option => $command) {
            $option_input = $input->getOption($option);

            if ($option_input !== FALSE) {
                $option_name = Inflector::classify(str_ireplace($option, '', ($option_input === NULL ? $name : $option_input))) . ucfirst($option);

                $option_data[$option_stub_list[$option]] = $option_name;
                $this->getApplication()->find($command)->run(
                    new ArrayInput(['name' => $option_name]),
                    $output
                );
            }
        }

        return $this->make($this->generateClassName($name), $output, $option_data);
    }

    protected function make($name, OutputInterface $output, array $stub_data = []): int
    {
        $stub_data[$this->stub_search_class] = $name;
        $stub_data[$this->stub_search_parent_class] = $this->checkParentFile('core', TRUE);

        return $this->helperGenerate($name, $output, $stub_data);
    }
}
