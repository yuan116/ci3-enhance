<?php

namespace Yuan116\Ci3\Enhance\Consoles\Makers;

defined('CI_VERSION') or die('Only for Codeigniter 3 (CI3)');

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\{
    InputArgument,
    InputInterface
};
use Symfony\Component\Console\Output\OutputInterface;
use Yuan116\Ci3\Enhance\Consoles\ConsoleHelperTrait;

class Migration extends Command
{
    use ConsoleHelperTrait;

    protected $migration;

    public function __construct()
    {
        parent::__construct('make:migration');
        $this->stub_file = 'migration';
        $this->suffix = 'Migration';
        $this->setMigrationInstance()->setAppFileSystemInstance()->setVendorFileSystemInstance();
        $this->path = basename($this->migration->getMigrationPath());
    }

    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this->setDescription('Create a new Migration')
            ->addArgument(
                'name',
                InputArgument::REQUIRED,
                "{$this->suffix} class name, by default will add suffix '{$this->suffix}' at class name."
            );
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = str_ireplace($this->suffix, '', $input->getArgument('name'));

        return $this->make($this->generateClassName($name), $output);
    }

    protected function make($name, OutputInterface $output): int
    {
        $stub_data[$this->stub_search_class] = $name;
        $stub_data[$this->stub_search_parent_class] = $this->checkParentFile('libraries', TRUE);

        if (!$this->migrationExist($name)) {
            $file_name = $this->getMigrationPrefixTypeValue() . '_' . $name;

            return $this->writeStub(
                $this->path . DIRECTORY_SEPARATOR . $file_name . '.php',
                $stub_data,
                $output
            );
        } else {
            $output->writeln("<error>{$this->suffix} '{$name}' already exists. Please change another name.</error>");
            return Command::FAILURE;
        }
    }

    protected function migrationExist($name)
    {
        return !empty(glob(APPPATH . $this->path . DIRECTORY_SEPARATOR . "*_{$name}.php", GLOB_MARK));
    }

    protected function getMigrationPrefixTypeValue()
    {
        $migration_type = $this->migration->getMigrationType();

        if ($migration_type === 'timestamp') {
            return date('YmdHis');
        }

        $migration_file_list = $this->migration->find_migrations();

        $value = (int) (empty($migration_file_list) ? 0 : array_key_last($migration_file_list));

        return str_pad(++$value, 3, '0', STR_PAD_LEFT);
    }
}
