<?php

namespace Yuan116\Ci3\Enhance\Consoles\Makers;

defined('CI_VERSION') or die('Only for Codeigniter 3 (CI3)');

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\{
    InputArgument,
    ArrayInput,
    InputInterface,
    InputOption
};
use Symfony\Component\Console\Output\OutputInterface;
use Yuan116\Ci3\Enhance\Consoles\ConsoleHelperTrait;
use Yuan116\Ci3\Enhance\Helpers\Inflector;

class Model extends Command
{
    use ConsoleHelperTrait;

    protected $option_list = [
        'migration' => 'make:migration',
        'seed' => 'make:seeder',
    ];

    public function __construct()
    {
        parent::__construct('make:model');
        $this->path = 'models';
        $this->stub_file = 'model';
        $this->suffix = 'Model';
        $this->setAppFileSystemInstance()->setVendorFileSystemInstance();
    }

    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this->setDescription('Create a new Model')
            ->addArgument(
                'name',
                InputArgument::REQUIRED,
                "{$this->suffix} class name, by default will add suffix '{$this->suffix}' at class name."
            )
            ->addOption(
                'migration',
                'm',
                InputOption::VALUE_OPTIONAL,
                'Migration class name, by default will add prefix "Migration" at class name. 
                If no value apply will auto set Migration class name as Create{ModelName}Migration class name',
                FALSE
            )
            ->addOption(
                'seeder',
                's',
                InputOption::VALUE_OPTIONAL,
                'Seeder class name, by default will add prefix "Seeder" at class name. 
                If no value apply will auto set Seeder class name as {ModelName}Seeder class name',
                FALSE
            );;
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = str_ireplace($this->suffix, '', $input->getArgument('name'));

        $option_input = $input->getOption('migration');

        if ($option_input !== FALSE) {
            $this->getApplication()->find('make:migration')->run(
                new ArrayInput(['name' => 'Create' . Inflector::classify(str_ireplace('migration', '', ($option_input === NULL ? $name : $option_input)))]),
                $output
            );
        }

        $option_input = $input->getOption('seeder');

        if ($option_input !== FALSE) {
            $this->getApplication()->find('make:seeder')->run(
                new ArrayInput(['name' => Inflector::classify(str_ireplace('seeder', '', ($option_input === NULL ? $name : $option_input)))]),
                $output
            );
        }

        return $this->make($this->generateClassName($name), $output);
    }

    protected function make($name, OutputInterface $output): int
    {
        $stub_data[$this->stub_search_class] = $name;
        $stub_data[$this->stub_search_parent_class] = $this->checkParentFile('core', TRUE);

        return $this->helperGenerate($name, $output, $stub_data);
    }
}
