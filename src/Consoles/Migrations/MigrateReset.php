<?php

namespace Yuan116\Ci3\Enhance\Consoles\Migrations;

defined('CI_VERSION') or die('Only for Codeigniter 3 (CI3)');

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\{
    ArrayInput,
    InputInterface
};
use Symfony\Component\Console\Output\OutputInterface;
use Yuan116\Ci3\Enhance\Consoles\ConsoleHelperTrait;

class MigrateReset extends Command
{
    use ConsoleHelperTrait;

    protected $migration;

    public function __construct()
    {
        parent::__construct('migrate:reset');
        $this->setMigrationInstance();
    }

    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this->setDescription('Rollback all migration');
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        return $this->getApplication()->find('migrate:rollback')->run(
            new ArrayInput(['--step' => $this->migration->getBatch()]),
            $output
        );
    }
}
