<?php

namespace Yuan116\Ci3\Enhance\Consoles\Migrations;

defined('CI_VERSION') or die('Only for Codeigniter 3 (CI3)');

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\{
    InputInterface,
    InputOption
};
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Yuan116\Ci3\Enhance\Consoles\ConsoleHelperTrait;

class MigrateRollback extends Command
{
    use ConsoleHelperTrait;

    protected $migration;

    public function __construct()
    {
        parent::__construct('migrate:rollback');
        $this->setMigrationInstance();
    }

    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this->setDescription('Rollback migration')
            ->addOption(
                'step',
                's',
                InputOption::VALUE_REQUIRED,
                'Number of migration to rollback.',
                0
            );
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $formatter_helper = $this->getHelper('formatter');
        $question_helper = $this->getHelper('question');

        $step = $input->getOption('step');
        $last_batch = (int) $this->migration->getBatch();

        if ($last_batch < 1) {
            $output->writeln($formatter_helper->formatBlock('Nothing to rollback.', 'comment'));

            return Command::SUCCESS;
        }

        if ($step !== NULL && $step < 1 && $step > $last_batch) {
            $output->writeln($formatter_helper->formatBlock('--step must be positive integer value and smaller or equal to last migration\'s batch.', 'error'));

            return Command::FAILURE;
        }

        $batch = $last_batch - ($step === 0 ? 1 : $step);
        $question = new ConfirmationQuestion("Rollback to batch {$batch}? [y/n]: ", FALSE);

        if (!$question_helper->ask($input, $output, $question)) {
            return Command::SUCCESS;
        }

        $migrated_file_list = $this->migration->getMigrationList(['batch >' => $batch]);
        krsort($migrated_file_list);

        if (empty($migrated_file_list)) {
            $output->writeln($formatter_helper->formatBlock('Nothing to rollback.', 'comment'));

            return Command::SUCCESS;
        }

        return $this->rollback($migrated_file_list, $output, $formatter_helper);
    }

    protected function rollback(array $migrated_file_list, OutputInterface $output, $formatter_helper): int
    {
        $path = $this->migration->getMigrationPath();
        $total_time = $total_migrated = 0;

        $this->migration->transStart();

        foreach ($migrated_file_list as $row) {
            $output->writeln("<comment>Rolling back</comment>: {$row->filename} ...");
            $start = microtime(true);

            require_once $path . $row->filename . '.php';
            $filename_list = explode('_', $row->filename);
            array_shift($filename_list);
            $class = implode('_', $filename_list);
            call_user_func([new $class, 'down']);

            $this->migration->rollback(['id' => $row->id]);
            $this->migration->transCommit();

            $end = microtime(true) - $start;
            $total_time += $end;
            $total_migrated++;
            $output->writeln("<info>Rollbacked</info>: {$row->filename} ." . $this->calculateTimeUsage($end));
        }

        $output->writeln($formatter_helper->formatBlock("Successfully rollback: {$total_migrated} file(s). Total " . $this->calculateTimeUsage($total_time), 'info'));

        return Command::SUCCESS;
    }
}
