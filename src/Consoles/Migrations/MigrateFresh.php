<?php

namespace Yuan116\Ci3\Enhance\Consoles\Migrations;

defined('CI_VERSION') or die('Only for Codeigniter 3 (CI3)');

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\{
    InputInterface,
    InputOption
};
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Yuan116\Ci3\Enhance\Consoles\ConsoleHelperTrait;

class MigrateFresh extends Command
{
    use ConsoleHelperTrait;

    protected $migration;

    public function __construct()
    {
        parent::__construct('migrate:fresh');
        $this->setMigrationInstance();
    }

    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this->setDescription('Drop all tables and run migration')
            ->addOption(
                'seed',
                NULL,
                InputOption::VALUE_OPTIONAL,
                'Seeder class name to run. (By default run all seeder)',
                FALSE
            );
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $formatter_helper = $this->getHelper('formatter');
        $question_helper = $this->getHelper('question');

        $question = new ConfirmationQuestion("Confirm to run fresh command? It will drop all tables and run migration. [y/n]: ", FALSE);

        if (!$question_helper->ask($input, $output, $question)) {
            return Command::SUCCESS;
        }

        $total_time = 0;
        $table_list = array_diff($this->migration->getTableList(), [$this->migration->getMigrationTable(), 'ci_log_viewer_auth']);

        foreach ($table_list as $table) {
            $start = microtime(true);
            $this->migration->dropTable($table);
            $total_time += microtime(true) - $start;
        }
        $output->writeln($formatter_helper->formatBlock('Dropped ' . count($table_list) . ' table(s). Total ' . $this->calculateTimeUsage($total_time), 'info'));

        $this->migration->rollback(['batch >' => 0]);

        return $this->getApplication()->find('migrate')->run($input, $output);
    }
}
