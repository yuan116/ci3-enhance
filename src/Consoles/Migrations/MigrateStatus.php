<?php

namespace Yuan116\Ci3\Enhance\Consoles\Migrations;

defined('CI_VERSION') or die('Only for Codeigniter 3 (CI3)');

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Yuan116\Ci3\Enhance\Consoles\ConsoleHelperTrait;

class MigrateStatus extends Command
{
    use ConsoleHelperTrait;

    protected $migration;

    public function __construct()
    {
        parent::__construct('migrate:status');
        $this->setMigrationInstance();
    }

    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this->setDescription('Check migration status');
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $migration_file_list = $this->migration->getMigrationFileList();
        $migrated_file_list = array_column($this->migration->getMigrationList(), 'batch', 'filename');

        $table = new Table($output);
        $table->setHeaders(['No.', 'Filename', 'Batch', 'Ran?']);

        $count = 1;
        $table_body_list = [];
        foreach ($migration_file_list as $filename) {
            $batch = $migrated_file_list[$filename] ?? NULL;

            $table_body_list[] = [
                $count++,
                $filename,
                $batch,
                ($batch !== NULL) ? '<info>Yes</info>' : '<comment>No</comment>'
            ];
        }
        $table->setRows($table_body_list);
        $table->render();

        return Command::SUCCESS;
    }
}
