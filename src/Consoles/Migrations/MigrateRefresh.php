<?php

namespace Yuan116\Ci3\Enhance\Consoles\Migrations;

defined('CI_VERSION') or die('Only for Codeigniter 3 (CI3)');

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\{
    ArrayInput,
    InputInterface,
    InputOption
};
use Symfony\Component\Console\Output\OutputInterface;
use Yuan116\Ci3\Enhance\Consoles\ConsoleHelperTrait;

class MigrateRefresh extends Command
{
    use ConsoleHelperTrait;

    protected $migration;

    public function __construct()
    {
        parent::__construct('migrate:refresh');
        $this->setMigrationInstance();
    }

    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this->setDescription('Rollback all migration and run migration')
            ->addOption(
                'seed',
                NULL,
                InputOption::VALUE_OPTIONAL,
                'Seeder class name to run. (By default run all seeder)',
                FALSE
            );
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->getApplication()->find('migrate:rollback')->run(
            new ArrayInput(['--step' => $this->migration->getBatch()]),
            $output
        );

        return $this->getApplication()->find('migrate')->run($input, $output);
    }
}
