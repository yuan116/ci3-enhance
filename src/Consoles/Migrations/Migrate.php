<?php

namespace Yuan116\Ci3\Enhance\Consoles\Migrations;

defined('CI_VERSION') or die('Only for Codeigniter 3 (CI3)');

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\{
    ArrayInput,
    InputInterface,
    InputOption
};
use Symfony\Component\Console\Output\OutputInterface;
use Yuan116\Ci3\Enhance\Consoles\ConsoleHelperTrait;

class Migrate extends Command
{
    use ConsoleHelperTrait;

    protected $migration;

    public function __construct()
    {
        parent::__construct('migrate');
        $this->setMigrationInstance();
    }

    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this->setDescription('Run migration')
            ->addOption(
                'seed',
                NULL,
                InputOption::VALUE_OPTIONAL,
                'Seeder class name to run. (By default run all seeder)',
                FALSE
            );
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $migration_file_list = $this->migration->getMigrationFileList();
        $migrated_file_list = array_column($this->migration->getMigrationList(), 'filename');
        $unmigrated_file_list = array_diff($migration_file_list, $migrated_file_list);

        if (empty($unmigrated_file_list)) {
            $formatter = $this->getHelper('formatter');
            $output->writeln($formatter->formatBlock('Nothing to migrate.', 'comment'));

            $status = Command::SUCCESS;
        } else {
            $status = $this->migrate($unmigrated_file_list, $output);
        }

        $option_input = $input->getOption('seed');

        if ($option_input !== FALSE) {
            $this->getApplication()->find('seed')->run(
                new ArrayInput(['--class' => $option_input]),
                $output
            );
        }

        return $status;
    }

    protected function migrate(array $unmigrated_file_list, OutputInterface $output): int
    {
        $batch = $this->migration->getBatch() + 1;
        $path = $this->migration->getMigrationPath();
        $datetime = date('Y-m-d H:i:s');
        $total_time = $total_migrated = 0;

        foreach ($unmigrated_file_list as $prefix_value => $filename) {
            $this->migration->transStart();

            $output->writeln("<comment>Migrating</comment>: {$filename} ...");
            $start = microtime(true);

            require_once $path . $filename . '.php';
            $class = str_replace($prefix_value . '_', '', $filename);
            call_user_func([new $class, 'up']);

            $this->migration->run([
                'filename' => $filename,
                'batch' => $batch,
                'datetime' => $datetime
            ]);
            $this->migration->transCommit();

            $end = microtime(true) - $start;
            $total_time += $end;
            $total_migrated++;
            $output->writeln("<info>Migrated</info>: {$filename} ." . $this->calculateTimeUsage($end));
        }

        $formatter = $this->getHelper('formatter');
        $output->writeln($formatter->formatBlock("Successfully migrated: {$total_migrated} file(s). Total " . $this->calculateTimeUsage($total_time), 'info'));

        return Command::SUCCESS;
    }
}
