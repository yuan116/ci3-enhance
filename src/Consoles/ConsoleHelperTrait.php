<?php

namespace Yuan116\Ci3\Enhance\Consoles;

defined('CI_VERSION') or die('Only for Codeigniter 3 (CI3)');

use League\Flysystem\{
    Filesystem,
    UnableToRetrieveMetadata,
    UnableToWriteFile
};
use League\Flysystem\Local\LocalFilesystemAdapter;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Output\OutputInterface;
use Yuan116\Ci3\Enhance\Helpers\Inflector;
use Yuan116\Ci3\Enhance\Libraries\{
    MY_Migration,
    Seeder
};
use CompileError;
use UnexpectedValueException;

trait ConsoleHelperTrait
{
    protected $stub_search_class = '{__CLASS__}';
    protected $stub_search_parent_class = '{__PARENT_CLASS__}';
    protected $stub_search_model = '{__MODEL__}';
    protected $stub_search_route = '{__ROUTE__}';
    protected $stub_search_rule = '{__RULE__}';
    protected $stub_search_seed_table = '{__SEED_TABLE__}';
    protected $stub_search_seed_query = '{__SEED_QUERY__}';
    protected $stub_search_seed_classes = '{__SEED_CLASSES__}';

    protected $stub_file_controller = 'controller';
    protected $stub_file_route = 'route';
    protected $stub_file_rule = 'rule';
    protected $stub_file_seeder = 'seeder';

    protected $path = '';
    protected $stub_file = '';
    protected $suffix = '';

    protected $default_ci_subclass_prefix = 'MY_';

    protected function getStubSearchList(): array
    {
        return [
            $this->stub_search_class => '',
            $this->stub_search_parent_class => '',
            $this->stub_search_model => '',
            $this->stub_file_route => '',
            $this->stub_file_rule => '',
        ];
    }

    protected function generateClassName($name): string
    {
        return Inflector::classify($name) . $this->suffix;
    }

    protected function checkParentFile(string $directory, bool $has_subfix = TRUE)
    {
        $subclass_prefix = $has_subfix ? config_item('subclass_prefix') : '';

        if ($ci_class_found = is_file($ci_file = BASEPATH . $directory . DIRECTORY_SEPARATOR . $this->suffix . '.php')) {
            require_once $ci_file;
        }

        $namespace_directory = ucfirst($directory);
        $namespaced_class_found = $this->vendor_file_system->fileExists($namespace_directory . DIRECTORY_SEPARATOR . $this->default_ci_subclass_prefix . $this->suffix . '.php');

        if ($namespaced_class_found) {
            $namespaced_class = "\\Yuan116\\Ci3\\Enhance\\{$namespace_directory}\\{$this->default_ci_subclass_prefix}{$this->suffix}";
        }

        if ($this->app_file_system->fileExists($path = $directory . DIRECTORY_SEPARATOR . $subclass_prefix . $this->suffix  . '.php')) {
            require_once APPPATH . $path;
            $subclass = $subclass_prefix . $this->suffix;

            if ($namespaced_class_found) {
                if (is_subclass_of($subclass, $namespaced_class)) {
                    return $subclass;
                }

                return $namespaced_class;
            }

            return $subclass;
        }

        if ($namespaced_class_found) {
            return $namespaced_class;
        }

        if ($ci_class_found) {
            return 'CI_' . $this->suffix;
        }

        throw new CompileError("Failed to open stream: No such file or directory - {$directory}/{$this->suffix}.php");
    }

    protected function helperGenerate($name, OutputInterface $output, array $stub_data = [])
    {
        if (!$this->app_file_system->fileExists($this->path)) {
            $this->app_file_system->createDirectory($this->path);
        }

        if (!$this->app_file_system->fileExists($file_path = $this->path . DIRECTORY_SEPARATOR . "{$name}.php")) {
            return $this->writeStub(
                $file_path,
                $stub_data,
                $output
            );
        } else {
            $output->writeln("<error>{$this->suffix} '{$name}' already exists.</error>");
            return Command::FAILURE;
        }
    }

    protected function writeStub(string $path, array $replace_list, OutputInterface $output)
    {
        $file_content = $this->getStubContent();
        foreach (array_replace($this->getStubSearchList(), $replace_list) as $search => $value) {
            $file_content = str_replace($search, $value, $file_content);
        }

        $name = basename($path);
        try {
            $this->app_file_system->write($path, $file_content);
            $output->writeln("<info>{$this->suffix} '{$name}' created successfully.</info>");
            return Command::SUCCESS;
        } catch (UnableToWriteFile $exception) {
            $output->writeln("<error>Fail to create {$this->suffix} '{$name}'. {$exception->getMessage()}</error>");
            return Command::FAILURE;
        }
    }

    protected function getStubContent(): string
    {
        if (empty($this->stub_file)) {
            throw new UnexpectedValueException(get_class($this) . '::stub_file cannot have empty value.');
        }

        $file = 'consoles' . DIRECTORY_SEPARATOR . 'stubs' . DIRECTORY_SEPARATOR . $this->stub_file . '.stub';
        if (!$this->vendor_file_system->fileExists($file)) {
            throw new UnableToRetrieveMetadata("Stub file missing: {$file}");
        }

        return $this->vendor_file_system->read($file);
    }

    protected function setAppFileSystemInstance()
    {
        $this->app_file_system = new Filesystem(new LocalFilesystemAdapter(APPPATH));

        return $this;
    }

    protected function setVendorFileSystemInstance()
    {
        $this->vendor_file_system = new Filesystem(new LocalFilesystemAdapter(dirname(__DIR__)));

        return $this;
    }

    protected function setMigrationInstance()
    {
        $ci = get_instance();
        $ci->load->library('migration');

        if ($ci->migration instanceof MY_Migration) {
            $this->migration = $ci->migration;
        } else {
            $this->migration = new MY_Migration();
        }

        return $this;
    }

    protected function setSeederInstance()
    {
        if (file_exists(APPPATH . 'libraries' . DIRECTORY_SEPARATOR . 'Seeder.php')) {
            $ci = get_instance();
            $ci->load->library('seeder');
            $this->seeder = $ci->seeder;
        } else {
            $this->seeder = new Seeder();
        }

        return $this;
    }

    protected function calculateTimeUsage(float $seconds): string
    {
        if (($seconds % 60) === 0) {
            $ms = number_format($seconds * 1000, 2);
            return " {$ms}ms";
        }

        $hours = floor($seconds / 3600);
        $minutes = floor(($seconds / 60) % 60);
        $seconds = round($seconds % 60, 2);

        $str = '';
        if ($hours > 0) {
            $str .= $hours . 'h';
        }

        if ($str !== '' || $minutes > 0) {
            $str .= " {$minutes}m";
        }

        return trim("{$str} {$seconds}s");
    }
}
