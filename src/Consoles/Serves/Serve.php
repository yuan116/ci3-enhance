<?php

namespace Yuan116\Ci3\Enhance\Consoles\Serves;

defined('CI_VERSION') or die('Only for Codeigniter 3 (CI3)');

use League\Flysystem\{
    Filesystem,
    UnableToRetrieveMetadata,
    UnableToWriteFile
};
use League\Flysystem\Local\LocalFilesystemAdapter;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\{
    InputArgument,
    InputInterface
};
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\{
    PhpExecutableFinder,
    Process
};
use Yuan116\Ci3\Enhance\Consoles\ConsoleHelperTrait;

class Serve extends Command
{
    use ConsoleHelperTrait;

    protected Filesystem $root_file_system;

    public function __construct()
    {
        parent::__construct('serve');
        $this->root_file_system = new Filesystem(new LocalFilesystemAdapter(FCPATH));
        $this->setVendorFileSystemInstance();
    }

    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this->setDescription('Serve the application on the PHP development server')
            ->addArgument(
                'host',
                InputArgument::OPTIONAL,
                'The host address to serve the application [default: "127.0.0.1"]',
                '127.0.0.1'
            )
            ->addArgument(
                'port',
                InputArgument::OPTIONAL,
                'The port to serve the application [default: "8000"]',
                8000
            );
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->root_file_system->fileExists('server.php')) {
            $file = 'consoles' . DIRECTORY_SEPARATOR . 'stubs' . DIRECTORY_SEPARATOR . 'server.stub';

            if (!$this->vendor_file_system->fileExists($file)) {
                throw new UnableToRetrieveMetadata("Stub file missing: {$file}");
            }

            try {
                $this->root_file_system->write('server.php', $this->vendor_file_system->read($file));
                $output->writeln("<info>'server.php' generated successfully.</info>");
            } catch (UnableToWriteFile $exception) {
                $output->writeln("<error>Fail to generate 'server.php'. {$exception->getMessage()}</error>");
                return Command::FAILURE;
            }
        }

        $process = new Process([
            (new PhpExecutableFinder)->find(FALSE),
            '-S',
            $input->getArgument('host') . ':' . $input->getArgument('port'),
            FCPATH . 'server.php'
        ]);

        $process->setTimeout(NULL);

        $process->run(function ($type, $buffer) use ($output) {
            $output->writeln($buffer);
        });

        return $process->getExitCode();
    }
}
