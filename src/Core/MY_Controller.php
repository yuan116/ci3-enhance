<?php

namespace Yuan116\Ci3\Enhance\Core;

defined('CI_VERSION') or die('Only for Codeigniter 3 (CI3)');

class MY_Controller extends \CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
}
