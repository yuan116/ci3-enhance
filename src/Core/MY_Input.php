<?php

namespace Yuan116\Ci3\Enhance\Core;

defined('CI_VERSION') or die('Only for Codeigniter 3 (CI3)');

class MY_Input extends \CI_Input
{
    /**
     * contain all post, put, patch, delete request
     */
    protected $request_input = [];

    public function __construct()
    {
        $raw_content = $this->raw_input_stream;
        if ($raw_content !== '') {
            $this->request_input = json_decode($raw_content, TRUE);

            if ($this->request_input === NULL) {
                parse_str($raw_content, $this->request_input);
            }

            $_POST = array_merge($_POST, $this->request_input);
        }

        parent::__construct();
    }

    protected function _sanitize_globals()
    {
        parent::_sanitize_globals();

        foreach ($this->request_input as $key => $val) {
            $this->request_input[$this->_clean_input_keys($key)] = $this->_clean_input_data($val);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function input_stream($index = NULL, $xss_clean = NULL)
    {
        return $this->_fetch_from_array($this->request_input, $index, $xss_clean);
    }

    /**
     * {@inheritDoc}
     */
    public function method($upper = FALSE)
    {
        $method = $this->post('_method');
        if ($method === NULL) {
            $method = $this->server('HTTP_X_HTTP_METHOD_OVERRIDE') ?? $this->server('REQUEST_METHOD');
        }

        return ($upper ? strtoupper($method) : strtolower($method));
    }
}
