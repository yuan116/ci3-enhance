<?php

namespace Yuan116\Ci3\Enhance\Core;

defined('CI_VERSION') or die('Only for Codeigniter 3 (CI3)');

class MY_Router extends \CI_Router
{
    /**
     * {@inheritDoc}
     */
    protected function _parse_routes()
    {
        $method = $_POST['_method'] ?? NULL;
        if ($method === NULL) {
            $method = $_SERVER['HTTP_X_HTTP_METHOD_OVERRIDE'] ?? NULL;
        }

        if ($method !== NULL) {
            $_SERVER['REQUEST_METHOD'] = $method;
        }

        return parent::_parse_routes();
    }
}
