<?php

namespace Yuan116\Ci3\Enhance\Core;

defined('CI_VERSION') or die('Only for Codeigniter 3 (CI3)');

use Yuan116\Ci3\Enhance\Helpers\Inflector;
use Yuan116\Ci3\Enhance\Libraries\Model\{
    ModelManipulationTrait,
    ModelQueryBuilderTrait,
    ModelResultTrait,
    ModelSoftDeleteTrait,
    ModelUtilitiesTrait
};
use BadMethodCallException;

class MY_Model extends \CI_Model
{
    use ModelManipulationTrait, ModelQueryBuilderTrait, ModelResultTrait, ModelSoftDeleteTrait, ModelUtilitiesTrait;

    protected string $table = '';
    protected string $primary_key = '';
    protected bool $has_timestamp = FALSE;
    protected bool $soft_delete = FALSE;
    protected string $date_format = 'Y-m-d H:i:s';

    public const CREATED_AT = 'created_at';
    public const UPDATED_AT = 'updated_at';
    public const DELETED_AT = 'deleted_at';

    protected array $created_columns = [];
    protected array $updated_columns = [];
    protected array $deleted_columns = [];

    protected array $proctected_fields = [];

    protected bool $has_call_from = FALSE;
    protected string $alias = '';

    public function __construct()
    {
        parent::__construct();
        $this->setTimestamp();
    }

    protected function setDateFormat(string $format)
    {
        switch ($format) {
            case 'datetime':
                $this->date_format = 'Y-m-d H:i:s';
                break;
            case 'date':
                $this->date_format = 'Y-m-d';
                break;
            case 'int':
            default:
                $this->date_format = 'U';
                break;
        }

        return $this;
    }

    protected function setTimestamp()
    {
        $this->created_columns[static::CREATED_AT] =
            $this->updated_columns[static::UPDATED_AT] =
            $this->deleted_columns[static::DELETED_AT] = date($this->date_format);

        return $this;
    }

    public function __call($method, $arguments)
    {
        $property = Inflector::abbrToUnderscore($method, ['get', 'set']);

        if (strpos($method, 'get') !== FALSE) {
            return $this->$property;
        }

        if ((strpos($method, 'set') !== FALSE) && !empty($arguments)) {
            $this->$property = $arguments;
            return $this;
        }

        $class = get_class($this);
        throw new BadMethodCallException("Call to undefined method {$class}::{$method}()");
    }

    public function getTable(string $alias = ''): string
    {
        if ($alias !== '') {
            $this->alias = $alias;
            return $this->table . ' AS ' . $alias;
        }

        return $this->table;
    }

    public function getPrimaryKey(): string
    {
        if ($this->alias !== '') {
            return $this->alias . '.' . $this->primary_key;
        }

        return $this->primary_key;
    }

    public function getForeignKey(): string
    {
        $foreign_key = '';

        if ($this->alias !== '') {
            $foreign_key = $this->alias . '.';
        }

        return $foreign_key . Inflector::singularize($this->table) . '_' . $this->primary_key;
    }

    public function alias(string $alias)
    {
        $this->alias = $alias;
        $this->from($this->table . ' ' . $alias);

        return $this;
    }

    public function resetAlias(): void
    {
        $this->alias = '';
    }

    public function save(array $data, $id = NULL): int
    {
        if (empty($id)) {
            $this->insert($data);

            $id = $this->db->insert_id();
        } else {
            $this->update($data, $id);
        }

        return $id;
    }

    public function getValuePairList($key, $value, $where = NULL): array
    {
        $key = is_string($key) ? explode('as', strtolower($key)) : $key;
        $value = is_string($value) ? explode('as', strtolower($value)) : $value;

        $this->select(implode(' AS ', $key))->select(implode(' AS ', $value))->setWhere($where);

        return array_column($this->result(), trim(end($value)), trim(end($key)));
    }
}
